<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Data\Models\Message;
use App\Data\Repositories\MessageRepository;

class MessageRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('MessageRepository', function () {
            return new MessageRepository(new Message);
        });
    }
}
