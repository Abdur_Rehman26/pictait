<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Data\Models\Setting;
use App\Data\Repositories\SettingRepository;

class SettingRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('SettingRepository', function () {
            return new SettingRepository(new Setting);
        });
    }
}
