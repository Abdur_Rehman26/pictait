<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Data\Models\Dare;
use App\Data\Repositories\DareRepository;
use App\Data\Models\User;
use App\Data\Models\Post;
use App\Notifications\DareUpdatedNotification;
use App\Notifications\DareCreatedNotification;

class DareRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Dare::created(function($dare) {

            $dareRepository =  app('DareRepository');

            $event = new \StdClass();
            $dare = $dareRepository->findById($dare->id);
            $event->from = User::find((int)$dare->user_id);
            $event->from->file_path = userImagePath($event->from->image, $event->from);
            $event->notifying_object = $dare;
            
            if($dare){

                $event->to = User::find($dare->challenger_id);
                $event->to->file_path = userImagePath($event->to->image, $event->to);
                $event->text = 'challenged you for a dare';
                $event->action = 'dare_created';
                $event->to->notify(new DareCreatedNotification($event));

            }


            \Log::info(json_encode('Dare created'));


        });


        Dare::updated(function($dare) {

            $dareRepository =  app('DareRepository');

            if($dare->user_id == request()->user()->id){
                return false;
            }

            $event = new \StdClass();
            $dare = $dareRepository->findById($dare->id, true);
            $event->from = User::find((int)$dare->challenger_id);
            $event->from->file_path = userImagePath($event->from->image, $event->from);

            $event->notifying_object = $dare;
            
            if($dare){
                if(!empty($dare->challengerImage)){
                    $event->text = 'uploaded the photo';   
                    $event->action = 'image_uploaded';
                }else{
                    $event->action = 'dare_accepted';
                    $event->text = 'accepted the dare';   
                }

                $event->to = User::find($dare->user_id);
                $event->to->file_path = userImagePath($event->to->image, $event->to);
                $event->to->notify(new DareUpdatedNotification($event));

            }


            \Log::info(json_encode('Dare Updated'));

        });

    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('DareRepository', function () {
            return new DareRepository(new Dare);
        });

    }
}
