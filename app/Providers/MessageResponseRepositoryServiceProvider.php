<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Data\Models\MessageResponse;
use App\Data\Models\Message;
use App\Data\Models\User;
use App\Data\Repositories\MessageResponseRepository;
use App\Notifications\MessageCreatedNotification;

class MessageResponseRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $messageResponseRepository = app('MessageResponseRepository');

        MessageResponse::created(function($createdMessage) use ($messageResponseRepository) {

            $event = new \StdClass();
            $event->id = $createdMessage->_id;
            $event->from = User::find((int)$createdMessage->user_id);
            $event->from->file_path = userImagePath($event->from->image, $event->from);
            $messageResponse = $messageResponseRepository->findById($createdMessage->_id);
            $message = Message::find($createdMessage->message_id);

            $event->notifying_object = $messageResponse;
            
            $sendToId = array_diff($message->members, [$createdMessage->user_id]);
            $sendToId = reset($sendToId);

            $criteria = ['message_id' => $message->_id, 'user_id' => $createdMessage->user_id];

            $event->last_message = $messageResponseRepository->findByCriteria($criteria);
            
            if($messageResponse){

                $event->to = User::find($sendToId);
                $event->to->file_path = userImagePath($event->to->image, $event->to);
                $event->text = 'messaged on you post';
                $event->to->notify(new MessageCreatedNotification($event));

            }


        });
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('MessageResponseRepository', function () {
            return new MessageResponseRepository(new MessageResponse);
        });
    }
}
