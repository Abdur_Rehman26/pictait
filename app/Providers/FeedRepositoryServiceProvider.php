<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Data\Repositories\FeedRepository;
use App\Jobs\FeedCreatedJob;
use App\Data\Models\Feed;

class FeedRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Feed::created(function($feed) {


            FeedCreatedJob::dispatch($feed);
            // ->onQueue(config('queue.pre_fix').'feed-notification')


        });
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('FeedRepository', function () {
            return new FeedRepository(new Feed);
        });
    }
}
