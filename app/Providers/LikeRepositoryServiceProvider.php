<?php

namespace App\Providers;
use App\Data\Repositories\LikeRepository;
use App\Data\Models\Like;

use Illuminate\Support\ServiceProvider;

class LikeRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Like::created(function($like) {

                dd($like);

        });

        Like::updated(function($like) {

                dd($like);

        });

    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('LikeRepository', function () {
            return new LikeRepository(new Like);
        });

    }
}
