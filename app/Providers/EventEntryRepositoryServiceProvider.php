<?php

namespace App\Providers;
use App\Data\Repositories\EventEntryRepository;
use App\Data\Models\EventEntry;

use Illuminate\Support\ServiceProvider;

class EventEntryRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {

    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('EventEntryRepository', function () {
            return new EventEntryRepository(new EventEntry);
        });

    }
}
