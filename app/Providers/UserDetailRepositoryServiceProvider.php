<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Data\Models\UserDetail;
use App\Data\Repositories\UserDetailRepository;

class UserDetailRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('UserDetailRepository', function () {
            return new UserDetailRepository(new UserDetail);
        });
    }
}
