<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Data\Repositories\CommentRepository;
use App\Data\Models\Comment;
use App\Data\Models\User;
use App\Data\Models\Post;
use App\Notifications\CommentCreatedNotification;

class CommentRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {


        Comment::created(function($comment) {

            if($comment->user_id == request()->user()->id){
                return false;
            }

            $event = new \StdClass();
            $event->id = $comment->id;
            $post = Post::find($comment->post_id);
            $event->from = User::find((int)$comment->user_id);
            $event->from->file_path = userImagePath($event->from->image, $event->from);
            $event->notifying_object = $comment;
            
            if($post){
                $event->to = User::find($post->user_id);
                $event->to->file_path = userImagePath($event->to->image, $event->to);
                $event->text = 'commented on you post';
                $event->to->notify(new CommentCreatedNotification($event));

            }

        });


    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('CommentRepository', function () {
            return new CommentRepository(new Comment);
        });
    }
}
