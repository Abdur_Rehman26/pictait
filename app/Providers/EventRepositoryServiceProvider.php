<?php

namespace App\Providers;
use App\Data\Repositories\EventRepository;
use App\Data\Models\Event;

use Illuminate\Support\ServiceProvider;

class EventRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {

    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('EventRepository', function () {
            return new EventRepository(new Event);
        });

    }
}
