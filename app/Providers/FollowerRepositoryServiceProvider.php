<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Data\Models\Follower;
use App\Data\Repositories\FollowerRepository;

class FollowerRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
                $this->app->bind('FollowerRepository', function () {
            return new FollowerRepository(new Follower);
        });

    }
}
