<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Data\Models\Post;
use App\Data\Repositories\PostRepository;
use App\Data\Repositories\FeedRepository;

class PostRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $feedRepository = app('FeedRepository');

        Post::created(function($post) use ($feedRepository){

            $feedData = $feedRepository->composeData($post);

            $feedRepository->create($feedData);

        });
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('PostRepository', function () {
            return new PostRepository(new Post);
        });
    }
}
