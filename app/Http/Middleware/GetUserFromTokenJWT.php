<?php

namespace App\Http\Middleware;

use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Middleware\BaseMiddleware;
use Tymon\JWTAuth\Exceptions\TokenBlacklistedException;
use Closure;

class GetUserFromTokenJWT extends BaseMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
     public function handle($request, \Closure $next)
    {
        if (! $token = $this->auth->setRequest($request)->getToken()) {
            if ($request->ajax()) {
                $code = 400;
                $output =  ['code' => $code,'messages'=> 'token_not_provided'];
                return $this->respond('tymon.jwt.absent', $output, $code);
            } else {
                abort(400);
            }
        }

        try {
            $user = $this->auth->authenticate($token);
        } catch (TokenExpiredException $e) {
            if ($request->ajax()) {
                $code = $e->getStatusCode();
                $output =  ['code' => $code,'messages'=> 'token_expired'];
                return $this->respond('tymon.jwt.expired', $output, $code, [$e]);
            } else {
                abort($e->getStatusCode());
            }
        } catch (JWTException $e) {
            if ($request->ajax()) {
                $code = $e->getStatusCode();
                $output =  ['code' => $code,'messages'=> 'token_invalid'];
                return $this->respond('tymon.jwt.invalid', $output, $e->getStatusCode(), [$e]);
            } else {
                abort($e->getStatusCode());
            }
        }

        if (! $user) {
            $code = 404;
            $output =  ['code' => $code,'messages'=> 'user_not_found'];
            return $this->respond('tymon.jwt.user_not_found', $output, $code);
        }

        $this->events->fire('tymon.jwt.valid', $user);

        $request->user_id = $user->id;
        
        return $next($request);
    }
}
