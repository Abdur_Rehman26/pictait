<?php

namespace App\Http\Middleware;

use Closure;
use Session;
use JWTAuth;

class UserAuthMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = $request->session()->get('token');
        
        if(!empty($token)){
            return redirectTo('/');
        }
        
        return $next($request);
    }
}
