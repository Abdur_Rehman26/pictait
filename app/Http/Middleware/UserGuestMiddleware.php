<?php

namespace App\Http\Middleware;

use Closure;
use Session;
use JWTAuth;


class UserGuestMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $token = $request->session()->get('token');
        if(!$token){
            return redirect('/login');    
        }

        return $next($request);
    }
}
