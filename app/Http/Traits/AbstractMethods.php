<?php
namespace App\Http\Traits;

trait AbstractMethods{

	public function searchCriteria($input) {

		foreach ($input as $key => $value)
		{

			if (in_array($key, $this->model->searchables)) {
				$this->builder = is_string($input[$key]) ? $this->builder->where($key, 'like', '%' . $input[$key] . '%') : $this->builder->where($key, '=', $input[$key]);
			}

		}

		return $this->builder;

	}
}