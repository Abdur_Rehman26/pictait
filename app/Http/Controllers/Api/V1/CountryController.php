<?php

namespace App\Http\Controllers\Api\V1;

use App\Data\Repositories\CountryRepository;
use Kazmi\Http\Controllers\ApiResourceController;

class CountryController extends ApiResourceController{
    
    public $_repository;

    public function __construct(CountryRepository $repository){
        $this->_repository = $repository;
    }

    public function rules($value=''){
        $rules = [];

        if($value == 'store'){
            

        }

        if($value == 'update'){

            $rules['id'] =  'required';

        }


        if($value == 'destroy'){

            $rules['id'] =  'required';

        }

        if($value == 'show'){

            $rules['id'] =  'required';

        }

        if($value == 'index'){
         
            $rules['pagination'] =  'nullable|in:true,false';

        }

        return $rules;
    
    }

    public function input($value=''){
        $input = request()->only('id');
        
        return $input;
    }
}