<?php

namespace App\Http\Controllers\Api\V1;

use App\Data\Repositories\EventRepository;
use Kazmi\Http\Controllers\ApiResourceController;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\Request;
use App\Jobs\SendEventInvitesJob;


class EventController extends ApiResourceController{

    public $_repository;

    public function __construct(EventRepository $repository){
        $this->_repository = $repository;
    }

    public function rules($value=''){
        $rules = [];

        if($value == 'store'){


        }

        if($value == 'update'){

            $rules['id'] =  'required';

        }


        if($value == 'destroy'){

            $rules['id'] =  'required';

        }

        if($value == 'show'){

            $rules['id'] =  'required';

        }

        if($value == 'index'){

            $rules['pagination'] =  'nullable|in:true,false';

        }

        return $rules;

    }

    public function input($value=''){
        $input = request()->only(
            'id', 'description', 'name', 'image', 'entries', 'entry_status', 'status',
            'pagination', 'per_page', 'privacy_type' , 'event_type', 'number_of_images',
            'user_id'
        );

        if(empty($input['user_id'])){
            $input['user_id'] = request()->user()->id;      
        }

        return $input;
    }


    public function sendInvite(Request $request)
    {
        $input = $request->only('event_id', 'users');
        $input['user_id'] = request()->user()->id;

        SendEventInvitesJob::dispatch($input);

        $output = ['response' => ['message' => 'Success']];

        return response()->json($output, 200);

    }

}