<?php

namespace App\Http\Controllers\Api\V1;

use App\Data\Repositories\CommentRepository;
use Kazmi\Http\Controllers\ApiResourceController;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class CommentController extends ApiResourceController
{
  public $_repository;
  CONST PAGIATION = true , PER_PAGE = 5;


  public function __construct(CommentRepository $repository){
   $this->_repository = $repository;
 }

 public function rules($value='')
 {
   return [
     'post_id' => [
       'required'
     ],
   ];
 }

 public function input($value='')
 {
  $input = request()->only('post_id', 'comment' , 'page' , 'pagination');
  $input['user_id'] = request()->user()->id;
  return $input;
}

   //Get all records
public function index(Request $request)
{


  $rules = $this->rules(__FUNCTION__);
  $input = $this->input(__FUNCTION__);

  $this->validate($request, $rules);

  $per_page = self::PER_PAGE ? self::PER_PAGE : config('app.per_page');
  $pagination = !empty($input['pagination']) && $input['pagination'] == 'true' ? true : false; 
  
  $data = $this->_repository->findCollectionByCriteria($pagination, $per_page, $input);

  $output = ['response' => ['data' => $data['data'], 'pagination' => !empty($data['pagination']) ? $data['pagination'] : false   , 'message' => 'Success']];

        // HTTP_OK = 200;

  return response()->json($output, Response::HTTP_OK);

}


}