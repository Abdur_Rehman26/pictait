<?php

namespace App\Http\Controllers\Api\V1;

use App\Data\Repositories\UserRepository;
use Symfony\Component\HttpFoundation\Response;
use Kazmi\Http\Controllers\ApiResourceController;
use Illuminate\Http\Request;
use App\Data\Models\User;
use Kazmi\Helpers\Helper;
use Session;
use JWTAuth;


class UserController extends ApiResourceController
{

	public $_repository;

	public function __construct(UserRepository $repository){
		$this->_repository = $repository;
	}

	public function store(Request $request)
	{
		$credentials = $request->only('email' , 'provider_id' , 'image');

		if(empty($credentials['email'])){
			$request->request->add(['email' => request()->only('provider_id')['provider_id']]);
		}

		if(empty($credentials['email'])){
			$credentials['email'] = $credentials['provider_id'];
		}


		$user = User::where('email', '=' , $credentials['email'])->first();
        

		if (!empty($user)) {

			$user->access_token = $token = $user->createToken('Token Name')->accessToken;
        	$user->file_path = userImagePath($user->image, $user->id);
	
			if ($token) {
				
				Session::put('token', $token);
				$output = ['mesage' => 'success' , 'data' => ['token' => $token, 'user' => $user]];
				return response()->json($output , 200);
			}
		}

		parent::store($request);


		if($credentials['email']){
			$user = User::where('email', '=' , $credentials['email'])->first();
			$user->access_token = $token = $user->createToken('Token Name')->accessToken;
			$user->first_time_user = true;
			$user->file_path = userImagePath($user->image, $user->id);

			if ($token) {
				Session::put('token', $token);

				$output = ['mesage' => 'success' , 'data' => ['token' => $token, 'user' => $user]];
				return response()->json($output , 200);
			}
		}


	}

	public function rules($value='')
	{


		$rules = [];

		if($value == 'store'){

			$rules['email'] = 'required|unique:users,email';
			$rules['first_name'] = 'required';

		}

		if($value == 'update'){

			$rules['id'] =  'required|exists:users,id';

		}


		if($value == 'destroy'){

			$rules['id'] =  'required|exists:users,id';

		}

		if($value == 'show'){

			$rules['id'] =  'required|exists:users,id';

		}

		if($value == 'index'){

			$rules['pagination'] =  'nullable|in:true,false';

		}

		return $rules;

	}

	public function input($value='')
	{
		$input = request()->only('email' , 'keyword' , 'first_name'  ,  'provider_id' ,  'provider_access_token' , 'gender' , 'image' , 'old_password' , 'new_password' , 'confirm_password', 'name');

		$input['id'] = !empty(request()->user()) ? request()->user()->id : null;

		return $input;
	}

	public function updateImage(Request $request)
	{
		$rules = $this->rules(__FUNCTION__);
		$input = $this->input(__FUNCTION__);

		$this->validate($request, $rules);


		$input['image'] = $input['name'];

		unset($input['name']);

		$data = $this->_repository->update($input);

		$output = ['response' => ['data' => $data, 'message' => 'Record added successfully']];

		// HTTP_OK = 200;

		return response()->json($output, Response::HTTP_OK);
	}


	public function getAuthUser()
	{
		$user_id = request()->user()->id;

		$data = app('UserRepository')->findById($user_id);

		$output = ['response' => ['data' => $data, 'message' => 'Record added successfully']];

		return response()->json($output, Response::HTTP_OK); 
	}

	public function changePassword(Request $request)
	{
		$input = $this->input(__FUNCTION__);
		$rules = $this->rules(__FUNCTION__);
		$this->validate($request, $rules);

		if(request()->user()->password){

			if(!\Hash::check($input['old_password'], request()->user()->password)){
				$code = Response::HTTP_UNPROCESSABLE_ENTITY;

				$output = ['errors' => ['old_password' => ['The old password does not match']] , 'message' => 'The given data was invalid'];

				return response()->json($output, $code);

			}
		}
		$new_password = bcrypt($input['new_password']);
		$input = ['id' => request()->user()->id];
		$input['password'] = $new_password;

		$data = $this->_repository->update($input);

		$output = ['response' => ['data' => $data, 'message' => 'Record added successfully']];

		// HTTP_OK = 200;

		return response()->json($output, Response::HTTP_OK);


	}


	public function logout(Request $request)
	{
		///
		///				
		$output = ['response' => ['message' => 'Success']];
		return response()->json($output, Response::HTTP_OK);

	}


	public function index(Request $request)
	{
		 // $rules = $this->rules(__FUNCTION__);
		$input = $this->input(__FUNCTION__);
        // $this->validate($request, $rules);
		$per_page = self::PER_PAGE ? self::PER_PAGE : config('app.per_page');
		$pagination = !empty($input['pagination']) && $input['pagination'] == 'true' ? true : false; 

		$data = $this->_repository->findCollectionByCriteria($pagination, $per_page, $input);

		$output = ['response' => ['data' => $data['data'], 'pagination' => !empty($data['pagination']) ? $data['pagination'] : false   , 'message' => 'Success']];

        // HTTP_OK = 200;

		return response()->json($output, Response::HTTP_OK);
	}


	public function search($searchType)
	{
		$input = $this->input();
		$input['user_id'] = $input['id'];
		unset($input['id']);

		$data = $this->_repository->searchFollowersByUser($input);

		$output = ['response' => ['data' => $data['data'], 'pagination' => !empty($data['pagination']) ? $data['pagination'] : false   , 'message' => 'Success']];

        // HTTP_OK = 200;

		return response()->json($output, Response::HTTP_OK);

	}

}


