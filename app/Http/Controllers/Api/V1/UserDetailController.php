<?php

namespace App\Http\Controllers\Api\V1;

use Kazmi\Http\Controllers\ApiResourceController;
use App\Data\Repositories\UserDetailRepository;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\Request;

class UserDetailController extends ApiResourceController{

    public $_repository;

    public function __construct(UserDetailRepository $repository){
        $this->_repository = $repository;
    }

    public function rules($value=''){
        $rules = [];

        if($value == 'store'){


        }

        if($value == 'update'){

            $rules['id'] =  'required';

        }


        if($value == 'destroy'){

            $rules['id'] =  'required';

        }

        if($value == 'show'){

            $rules['id'] =  'required';

        }

        if($value == 'index'){

            $rules['pagination'] =  'nullable|in:true,false';

        }

        return $rules;

    }

    public function input($value=''){
        $input = request()->only( 'user_id' , 'id' , 'first_name' , 'email' , 'last_name' , 'date_of_birth' , 'gender' , 'country' , 'status' , 'bio');
        
        return $input;
    }


    public function index(Request $request)
    {
        $input = $this->input(__METHOD__);

        $id = !empty($input['user_id']) ? (int)$input['user_id'] : request()->user()->id;

        $data = $this->_repository->getSingle($id);
        
        $user = app('UserRepository')->findById($id);

        $data->file_path = $user ? $user->file_path : null; 

        $settings = app('SettingRepository')->findByAttribute('user_id' , $id);
        
        $data->canSeePosts = false;
        $data->canFollow = false;
        // will be used later once i am done with the following part.
        if(!$settings){
            $data->canSeePosts = true;    
            $data->canFollow = true;
        }

        if($settings){
            if($settings->posts['can_view'] == 'EO'){
                $data->canSeePosts = true;    
            }

            if($settings->friend_requests['can_send'] == 'EO'){
                $data->canFollow = true;
            }
        }

        if($id == request()->user()->id){
            $data->canFollow = true;
            $data->canSeePosts = true;    
        }


        $criteria = ['user_id' => request()->user()->id , 'follow_id' =>  (int) $id , 'type' => 'user' , 'deleted_at' => null];

        $follow =app('FollowerRepository')->findByCriteria($criteria);

        $data->isFollowing = false;
        $data->isRequested = false;

        if(!empty($follow)){

            $data->isFollowing = !$follow->requested ? true : false; 
            $data->isRequested = $follow->requested ? true : false; 
        }

        $output = ['response' => ['data' => $data, 'message' => 'Success']];

        // HTTP_OK = 200;

        return response()->json($output, Response::HTTP_OK);

    }

}