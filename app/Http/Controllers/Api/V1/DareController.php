<?php

namespace App\Http\Controllers\Api\V1;

use App\Data\Repositories\DareRepository;
use Kazmi\Http\Controllers\ApiResourceController;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\Request;

class DareController extends ApiResourceController{

    public $_repository;

    CONST PAGINATION = true;
    CONST PER_PAGE = 20;

    public function __construct(DareRepository $repository){
        $this->_repository = $repository;
    }

    public function rules($value=''){
        $rules = [];

        if($value == 'store'){


        }

        if($value == 'update'){

            $rules['id'] =  'required';

        }


        if($value == 'destroy'){

            $rules['id'] =  'required';

        }

        if($value == 'show'){

            $rules['id'] =  'required';

        }

        if($value == 'index'){

            $rules['pagination'] =  'nullable|in:true,false';

        }

        return $rules;

    }

    public function input($value=''){
        $input = request()->only('status', 'per_page', 'user_id', 'id', 'liked', 'user_like_id', 'challenger_id', 'userImage', 'challengerImage', 'pagination', 'type', 'show_all', 'pin');

        if(empty($input['user_id'])){
            $input['user_id'] = request()->user() ? request()->user()->id : null; 
        }

        if(!empty($input['challenger_id'])){
            $input['challenger_id'] = (int) $input['challenger_id'];
        }

        $input['user_id'] = (int) $input['user_id'];
        
        if($value == 'update'){
            unset($input['user_id']);
        }

        return $input;

    }


    public function index(Request $request)
    {

        $input = $this->input('index');
        $rules = $this->rules('index');

        if($input['user_id'] != request()->user()->id){
            $settings = app('SettingRepository')->findByAttribute('user_id' , (int) $input['user_id']);

            if(!empty($settings) && $settings->posts['can_view'] == 'US'){

                $criteria = ['follow_id' => $input['user_id'] , 'type' => 'user', 'requested' => 0, 'user_id' => request()->user()->id];

                $follow =app('FollowerRepository')->findByCriteria($criteria);



                if(!$follow){

                    $code = 422;

                    $output = ['error' => [] , 'message' => "You are not allowed to view this user's posts"];

                    return response()->json($output, $code);


                }
            }
        }
        return parent::index($request);
    }


    //Update single record
    public function dareLike(Request $request, $id)
    {   

        $request->request->add(['id' => $id]);
        
        $input = $this->input(__FUNCTION__);
        $rules = $this->rules(__FUNCTION__);
        
        $messages = $this->messages(__FUNCTION__);

        $this->validate($request, $rules, $messages);

        $data = $this->_repository->dareLike($input);
        $output = ['response' => ['data' => $data, 'message' => $this->responseMessages(__FUNCTION__)]];

        // HTTP_OK = 200;

        return response()->json($output, Response::HTTP_OK);

    }


        //Get single record
    public function getUnreadCount(Request $request)
    {
        $rules = $this->rules(__FUNCTION__);
        $input = $this->input(__FUNCTION__);
        
        $this->validate($request, $rules);

        $criteria = ['challenger_id' => $input['user_id'] , 'status' => 'pending'];

        $data = $this->_repository->findCollectionByCriteria($criteria, true);

        $output = ['response' => ['data' => $data]];

        $code = 200;

        return response()->json($output, $code);

    }

        //Update single record
    public function pinToProfile(Request $request, $id)
    {   
        $request->request->add(['id' => $id]);
        
        $input = $this->input(__FUNCTION__);
        $rules = $this->rules(__FUNCTION__);
        
        $messages = $this->messages(__FUNCTION__);

        $this->validate($request, $rules, $messages);

        $data = $this->_repository->pinToProfile($input);
        $output = ['response' => ['data' => $data, 'message' => $this->responseMessages(__FUNCTION__)]];

        // HTTP_OK = 200;

        return response()->json($output, Response::HTTP_OK);

    }

}