<?php

namespace App\Http\Controllers\Api\V1;

use App\Data\Repositories\NotificationRepository;
use Kazmi\Http\Controllers\ApiResourceController;
use Illuminate\Http\Request;

class NotificationController extends ApiResourceController
{
    public $_repository;

    public function __construct(NotificationRepository $repository){
        $this->_repository = $repository;
    }

    public function rules($value='')
    {
        $rules = [
        ];

        return $rules;
    }

    public function input($value='')
    {
        $input = request()->only('pagination', 'type', 'id');
        $input['notifiable_id'] = request()->user()->id;
        return $input;
    }


    //Get single record
    public function getUnreadCount(Request $request)
    {
        $rules = $this->rules(__FUNCTION__);
        $input = $this->input(__FUNCTION__);

        $this->validate($request, $rules);


        $criteria = ['read_at' => null , 'notifiable_id' => $input['notifiable_id']];

        if(!empty($input['type'])){
            $criteria['type'] = $input['type'];
        }

        $data = $this->_repository->findCollectionByCriteria($criteria, true);

        $output = ['response' => ['data' => $data]];

        $code = 200;

        return response()->json($output, $code);

    }

    public function markAsRead(Request $request)
    {
        
        $rules = $this->rules(__FUNCTION__);
        $input = $this->input(__FUNCTION__);
        
        $this->validate($request, $rules);

        $input['read_at'] = null;

        $this->_repository->bulkUpdate($input);
        
        $output = ['response' => ['data' => null, 'success' => true]];

        $code = 200;

        return response()->json($output, $code);
    }

}