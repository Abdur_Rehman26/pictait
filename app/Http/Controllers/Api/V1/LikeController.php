<?php

namespace App\Http\Controllers\Api\V1;

use App\Data\Repositories\LikeRepository;
use Kazmi\Http\Controllers\ApiResourceController;
use Illuminate\Http\Request;
use Kazmi\Helpers\Helper;
use Symfony\Component\HttpFoundation\Response;

class LikeController extends ApiResourceController
{
  public $_repository;

  public function __construct(LikeRepository $repository){
   $this->_repository = $repository;
 }

  public function rules($value=''){
        $rules = [];

        if($value == 'store'){

            $rules['post_id'] =  'required';

        }

        if($value == 'update'){

            $rules['id'] =  'required';

        }


        if($value == 'show'){

            $rules['id'] =  'required';

        }

        if($value == 'index'){

            $rules['pagination'] =  'nullable|in:true,false';

        }

        return $rules;

    }


 public function input($value='')
 {
  $input = request()->only('post_id' , 'liked');
  $input['user_id'] = request()->user()->id;
  return $input;
}

public function store(Request $request)
{
  $rules = $this->rules(__FUNCTION__);
  $input = $this->input(__FUNCTION__);

  $this->validate($request, $rules);

  $data = app('PostRepository')->updateLikes($input);

  $output = ['response' => ['data' => $data, 'message' => 'Record added successfully']];

  return response()->json($output, Response::HTTP_OK);

}



    //Delete single record
    public function destroy(Request $request, $id)
    {
        $rules = $this->rules(__FUNCTION__);
        $input = $this->input(__FUNCTION__);

        $input['post_id'] = $id;

        $this->validate($request, $rules);

        $data = app('PostRepository')->updateLikes($input);

        $output = ['response' => ['data' => $data, 'message' => 'Success']];

        // HTTP_OK = 200;

        return response()->json($output, Response::HTTP_OK);

    }



}