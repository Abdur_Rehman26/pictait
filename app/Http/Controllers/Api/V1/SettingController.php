<?php

namespace App\Http\Controllers\Api\V1;

use Symfony\Component\HttpFoundation\Response;
use Kazmi\Http\Controllers\ApiResourceController;
use Illuminate\Http\Request;

use App\Data\Repositories\SettingRepository;

class SettingController extends ApiResourceController
{
    public $_repository;

   public function __construct(SettingRepository $repository){
       $this->_repository = $repository;
   }

   public function rules($value='')
   {
       return [
            'friend_requests' => [
               'required',
           ],
            'posts'=> [
               'required',
           ]
       ];
   }

   public function input($value='')
   {
      $input = request()->only('friend_requests' , 'posts');
      $input['user_id'] = request()->user()->id;
      return $input;
   }

    public function index(Request $request)
    {
        $input = $this->input(__METHOD__);
        
        $data = $this->_repository->findByAttribute('user_id' , $input['user_id']);

        $output = ['response' => ['data' => $data, 'message' => 'Success']];

        // HTTP_OK = 200;

        return response()->json($output, Response::HTTP_OK);

    }


}