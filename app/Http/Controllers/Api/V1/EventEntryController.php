<?php

namespace App\Http\Controllers\Api\V1;

use App\Data\Repositories\EventEntryRepository;
use Kazmi\Http\Controllers\ApiResourceController;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;


class EventEntryController extends ApiResourceController{

    public $_repository;

    public function __construct(EventEntryRepository $repository){
        $this->_repository = $repository;
    }

    public function rules($value=''){
        $rules = [];

        if($value == 'store'){


        }

        if($value == 'update'){

            $rules['id'] =  'required';

        }


        if($value == 'destroy'){

            $rules['id'] =  'required';

        }

        if($value == 'show'){

            $rules['id'] =  'required';

        }

        if($value == 'index'){

            $rules['pagination'] =  'nullable|in:true,false';

        }

        return $rules;

    }

    public function input($value=''){
        $input = request()->only('id', 'entries', 'event_id', 'pagination', 'liked', 'entry', 'status');
        $input['user_id'] = request()->user()->id;
        $input['event_id'] = (int) $input['event_id'];
     
        if($value == 'index'){
            if(!empty($input['status'])){
            
                $input['status'] = (int) $input['status'];
            
            }else{
                
                $input['status'] = 0;
            }
        }


        return $input;
    }

    public function store(Request $request)
    {
        $input = $this->input();
        $data = $this->_repository->composeEntryData($input);
        $this->_repository->bulkInsert($data);

        $code = 200;

        $output = ['data' => [] , 'message' => "Success"];

        return response()->json($output, $code);

    }

    public function uploadUserImage(Request $request)
    {
        $input = $this->input();
        $input['user_id'] = request()->user()->id;

        $request->request->add(['user_id' => $input['user_id']]);

        $rules = [
            'entry.name' => 'required',
            'event_id' => 'required|exists:events,id',
            'user_id' => ['required',
            Rule::unique('mongodb.event_entries')->where(function($query) use ($input){
                $query->where('event_id' , $input['event_id']);
            }),
        ]
    ];

    $this->validate($request, $rules);

    $input['status'] = 0;

    $data = $this->_repository->create($input);

    $output = [
        'response' => [
            'data' => $data,
            'message' => $this->responseMessages(__FUNCTION__),
        ]
    ];

        // HTTP_OK = 200;

    return response()->json($output, Response::HTTP_OK);

    }


    //Update single record
    public function updateLike(Request $request, $id)
    {   
        $request->request->add(['id' => $id]);
        
        $input = $this->input(__FUNCTION__);
        $rules = $this->rules(__FUNCTION__);
        
        $messages = $this->messages(__FUNCTION__);

        $this->validate($request, $rules, $messages);

        $data = $this->_repository->updateLike($input);
        $output = ['response' => ['data' => $data, 'message' => $this->responseMessages(__FUNCTION__)]];

        // HTTP_OK = 200;

        return response()->json($output, Response::HTTP_OK);

    }    //Update single record

    public function approve(Request $request, $id)
    {   
        $request->request->add(['id' => $id]);
        
        $input = $this->input(__FUNCTION__);
        $rules = $this->rules(__FUNCTION__);
        
        $messages = $this->messages(__FUNCTION__);

        $this->validate($request, $rules, $messages);

        $data = $this->_repository->update($input);
        $output = ['response' => ['data' => $data, 'message' => $this->responseMessages(__FUNCTION__)]];

        // HTTP_OK = 200;

        return response()->json($output, Response::HTTP_OK);

    }




}