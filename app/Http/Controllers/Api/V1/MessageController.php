<?php

namespace App\Http\Controllers\Api\V1;

use App\Data\Repositories\MessageRepository;
use Kazmi\Http\Controllers\ApiResourceController;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\Request;


class MessageController extends ApiResourceController
{
  public $_repository;

  public function __construct(MessageRepository $repository){
   $this->_repository = $repository;
 }

 public function rules($value='')
 {
   return [];   
 }

 public function input($value='')
 {
   $input = request()->only('id', 'pagination' , 'message' , 'receiver' , 'time', 'page');
   $input['pagination'] = true;
   $input['user_id'] = request()->user()->id;
   return $input;
 }



  /**
   * Display the specified resource.
   *
   * @param  int $id
   * @param Request $request
   * @return \Illuminate\Http\Response
   */
  public function show(Request $request , $id)
  {

    $rules = $this->rules(__FUNCTION__);
    $input = $this->input(__FUNCTION__);

    $this->validate($request, $rules);

    $per_page = self::PER_PAGE ? self::PER_PAGE : config('app.per_page');
    $pagination = true; 


    $user_id = $input['user_id'];
    $id = (int) $id;

    $recipient = app('UserRepository')->findById($id , false, false , false);

    $message = $this->_repository->findByUserId($user_id , $id);

    if($message)
    {
      $criteria = ['message_id'   =>  !empty($message->id) ?  $message->id : $message->_id];
      $messages = app('MessageResponseRepository')->findByAll($pagination, $per_page, $criteria);

      $criteria['user_id'] = $id;

      app('MessageResponseRepository')->bulkUpdate($criteria);

      $output = ['response' => [ 'data'  => $messages['data'], 'pagination'  => $messages['pagination'], 'additionalData' => ['message' => $message , 'recipient' => $recipient]]];

      return response()->json($output, Response::HTTP_OK);

    }

    $input =  ['type' => 'user' , 'members' => [ $user_id, $id ] , 'recipients' => [$id] ];
    $input['created_by'] = $user_id;

    $message = $this->_repository->create($input);

    $output = [
      'response' => [
        'data' => '',
        'pagination' => false, 
        'additionalData' => [
          'message' => $message,
          'recipient' => $recipient
        ]
      ]
    ];


    return response()->json($output, Response::HTTP_OK);
  }

  public function store(Request $request)
  {

    $rules = $this->rules(__FUNCTION__);
    $input = $this->input(__FUNCTION__);

    $this->validate($request, $rules);

    $data = $this->_repository->create($input);



    $output = ['response' => ['data' => $data, 'message' => 'Record added successfully']];

      // HTTP_OK = 200;

    return response()->json($output, Response::HTTP_OK);
  }
}