<?php

namespace App\Http\Controllers\Api\V1;

use App\Data\Repositories\FollowerRepository;
use Kazmi\Http\Controllers\ApiResourceController;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\Request;

class FollowerController extends ApiResourceController{

    public $_repository;

    CONST PAGINATION = true;
    CONST PER_PAGE = 20;

    public function __construct(FollowerRepository $repository){
        $this->_repository = $repository;
    }

    public function rules($value=''){
        $rules = [];

        if($value == 'store'){


        }

        if($value == 'update'){

            $rules['id'] =  'required';

        }


        if($value == 'destroy'){

            $rules['id'] =  'required';

        }

        if($value == 'show'){

            $rules['id'] =  'required';

        }

        if($value == 'index'){

            $rules['pagination'] =  'nullable|in:true,false';

        }

        return $rules;

    }

    public function input($value=''){
        $input = request()->only('requested', 'keyword', 'user_id', 'follow_id' , 'follow' , 'unfollow' , 'id' , 'type', 'pagination');

        if(empty($input['user_id'])){
            $input['user_id'] = request()->user()->id;
        }

        if($value == 'index'){

            if(!empty($input['requested']) && !empty($input['follow_id'])){
                $input['follow_id'] = (int)$input['follow_id'];
            }
            
            
            if(!empty($input['requested'])){
                $input['requested'] = (int)$input['requested'];
            }

            if(!empty($input['follow_id'])){
                unset($input['user_id']);
                $input['follow_id'] = (int)$input['follow_id'];
            }
            

            
        }

        if(!empty($input['user_id'])){
            $input['user_id']  = (int) $input['user_id'];
        }
        

        if($value != 'store' && $value != 'update'){

            if(!empty($input['type']) && $input['type'] == 'event'){
                unset($input['user_id']);
            }

            
        }

        return $input;
    }


    public function store(Request $request)
    {
        parent::store($request);
        
        $input = $this->input('store');

        $criteria = ['follow_id' => $input['follow_id'] , 'user_id' => $input['user_id'] , 'type' => $input['type'] , 'deleted_at' => null];
        
        $data = app('UserDetailRepository')->getSingle($input['follow_id'] , true);

        $follow =app('FollowerRepository')->findByCriteria($criteria, true);
        
        $data->isFollowing = false;
        $data->isRequested = false;

        if(!empty($follow)){

            $data->isFollowing = !$follow->requested ? true : false; 
            $data->isRequested = $follow->requested ? true : false; 
        }

        $output = ['response' => ['data' => $data, 'message' => 'Record added successfully']];

        // HTTP_OK = 200;

        return response()->json($output, Response::HTTP_OK);
    }

            //Get single record
    public function getUnreadCount(Request $request)
    {
        $rules = $this->rules(__FUNCTION__);
        $input = $this->input(__FUNCTION__);
        
        $this->validate($request, $rules);

        $criteria = ['follow_id' => $input['user_id'] , 'requested' => 1];

        $data = $this->_repository->findCollectionByCriteria($criteria, true);

        $output = ['response' => ['data' => $data]];

        $code = 200;

        return response()->json($output, $code);

    }


}