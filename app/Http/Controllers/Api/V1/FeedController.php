<?php

namespace App\Http\Controllers\Api\V1;

use App\Data\Repositories\FeedRepository;
use Kazmi\Http\Controllers\ApiResourceController;

class FeedController extends ApiResourceController
{
  public $_repository;
  CONST PAGIATION = true , PER_PAGE = 10;

  public function __construct(FeedRepository $repository){
   $this->_repository = $repository;
 }

 public function rules($value='')
 {
   return [];
 }

 public function input($value='')
 {
  $input = request()->only('pagination');
  $input['user_id'] = request()->user()->id;
  return $input;
}
}