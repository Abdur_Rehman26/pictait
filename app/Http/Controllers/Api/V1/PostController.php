<?php

namespace App\Http\Controllers\Api\V1;

use App\Data\Repositories\PostRepository;
use Kazmi\Http\Controllers\ApiResourceController;
use Illuminate\Http\Request;
use Kazmi\Helpers\Helper;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Validation\Rule;

class PostController extends ApiResourceController
{
    public $_repository;

    CONST PAGIATION = true , PER_PAGE = 10;

    public function __construct(PostRepository $repository){
        $this->_repository = $repository;
    }

    public function rules($value='')
    { 

        $rules = [];

        if($value == 'index') {



        }

        if($value == 'update'){

        }

        return $rules;

    }

    public function input($value='')
    {
        $input = request()->only('caption', 'image' , 'pagination' , 'user_id', 'id');
        $input['image'] = request()->file('image');
        $input['user_id'] = !empty($input['user_id'])  ? $input['user_id'] : request()->user()->id;
        $input['posted_by'] = 'user';
        $input['posted_by_id'] = $input['user_id'];
        $input["likeCount"] = 0;
        $input["likes"] = [];

        if($value != 'destroy' && $value != 'show'){
            unset($input['id']);
        }

        return $input;
    }

    public function store(Request $request)
    {
        $rules = $this->rules(__FUNCTION__);
        $input = $this->input(__FUNCTION__);

        $this->validate($request, $rules);

        $path = config('uploads.images.user.folder_name').'/'.$input['user_id'].'/posts/';

        $details = ['extensions' => [ 'jpg' , 'jpeg', 'png', 'gif', 'bmp', 'svg'] , 'path' => $path];

        $file = Helper::cropImage($input['image'] , $details);

        $input['file_name'] = $file['file_name'];
        $input['ext'] = $file['ext'];


        $data = $this->_repository->create($input);

        $output = ['response' => ['data' => $data, 'message' => 'Record added successfully']];

// HTTP_OK = 200;

        return response()->json($output, Response::HTTP_OK);

    }


    public function index(Request $request)
    {

        $input = $this->input('index');
        $rules = $this->rules('index');

        if($input['user_id'] != request()->user()->id){

            $settings = app('SettingRepository')->findByAttribute('user_id' , (int) $input['user_id']);

            if(!empty($settings) && $settings->posts['can_view'] == 'US'){

                $criteria = ['follow_id' => $input['user_id'] , 'type' => 'user' , 'requested' => 0, 'user_id' => request()->user()->id];

                $follow =app('FollowerRepository')->findByCriteria($criteria);

                if(!$follow){

                    $code = 422;

                    $output = ['error' => [] , 'message' => "You are not allowed to view this user's posts"];

                    return response()->json($output, $code);


                }
            }
        }


        return parent::index($request);
    }


}