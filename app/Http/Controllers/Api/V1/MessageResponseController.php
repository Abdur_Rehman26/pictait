<?php

namespace App\Http\Controllers\Api\V1;

use App\Data\Repositories\MessageResponseRepository;
use Kazmi\Http\Controllers\ApiResourceController;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\Request;

class MessageResponseController extends ApiResourceController{
    
    public $_repository;

    public function __construct(MessageResponseRepository $repository){
        $this->_repository = $repository;
    }

    public function rules($value=''){
        $rules = [];

        if($value == 'store'){
            

        }

        if($value == 'update'){

            $rules['id'] =  'required';

        }


        if($value == 'destroy'){

            $rules['id'] =  'required';

        }

        if($value == 'show'){

            $rules['id'] =  'required';

        }

        if($value == 'index'){
         
            $rules['pagination'] =  'nullable|in:true,false';

        }

        return $rules;
    
    }

    public function input($value=''){
        $input = request()->only('id' , 'recipient_id' , 'reply');
        $input['user_id'] = request()->user()->id;
        return $input;
    }

    //Create single record
    public function store(Request $request)
    {
        $rules = $this->rules(__FUNCTION__);
        $input = $this->input(__FUNCTION__);
        
        $this->validate($request, $rules);
        $data = $this->_repository->store($input);

        $output = ['response' => ['data' => $data, 'message' => 'Record added successfully']];

        // HTTP_OK = 200;

        return response()->json($output, Response::HTTP_OK);

    }

}