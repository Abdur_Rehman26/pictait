<?php

namespace App\Data\Repositories;

use Kazmi\Data\Repositories\AbstractRepository;
use Kazmi\Data\Contracts\RepositoryContract;
use App\Jobs\FollowerUpdatedJob;
use App\Data\Models\Follower;
use Carbon\Carbon;
use \Cache;
use DB;

class FollowerRepository extends AbstractRepository implements RepositoryContract
{
/**
*   
* These will hold the instance of Follower Class.
*
* @var object
* @access public
*
**/
public $model;

CONST PAGINATION = true;
CONST PER_PAGE = 20;

/**
*
* This is the prefix of the cache key to which the
* App\Data\Repositories data will be stored
* App\Data\Repositories Auto incremented Id will be append to it
*
* Example: Follower-1
*
* @var string
* @access protected
*
**/

protected $_cacheKey = 'Follower';
protected $_cacheTotalKey = 'total-Follower';

public function __construct(Follower $model)
{
    $this->model = $model;
    $this->builder = $model;

}

/**
*
* This method will fetch single model
* and will return output back to client as json
*
* @access public
* @return mixed
*
* @author Usaama Effendi <usaamaeffendi@gmail.com>
*
**/
public function findById($id, $refresh = false, $details = false, $encode = true) {

    $data = parent::findById($id, $refresh, $details, $encode);

    if($data){

        $data->formatted_created_at = Carbon::parse($data->created_at)->diffForHumans();

        $data->user = app('UserRepository')->findById($data->follow_id);
        
        $data->follower = app('UserRepository')->findById($data->user_id);

        $criteria = ['user_id' => $data->follow_id];

        $data->followers_count = $this->findCollectionByCriteria($criteria, true);
        
        $data->posts_count = app('PostRepository')->findCollectionByCriteria($criteria, true);


        $orCrtieria = [['follow_id' => $data->follow_id]];

        $data->dares_count = app('DareRepository')->findCollectionByCriteria($criteria, true, $orCrtieria);

        $data->formatted_friends_since = Carbon::parse($data->updated_at)->diffForHumans();



        return $data;
    }
    return null;
}

public function create(array $data = [])
{

    $criteria = ['follow_id' => $data['follow_id'] , 'user_id' => $data['user_id'] , 'type' => $data['type']];
    $criteria['deleted_at'] = Carbon::now()->toDateTimeString();
    $criteria['requested'] = false;

    if(!empty($data['follow'])){
        $criteria['deleted_at'] = null;
    }

    unset($data['follow']); 
    $userSettings =  app('SettingRepository')->findByAttribute('user_id' , $data['follow_id']);
    $criteria['updated_at'] = Carbon::now()->toDateTimeString();
    $criteria['created_at'] = Carbon::now()->toDateTimeString();

    if(!empty($userSettings)){
        if($userSettings->posts['can_view'] == 'EO'){
            $data = $this->model->insertOnDuplicateKey($criteria);
            return $data;
        }else{

            $criteria['requested'] = true;
        }              
    }

    $data = $this->model->insertOnDuplicateKey($criteria);
    FollowerUpdatedJob::dispatch($criteria)->onQueue(config('queue.pre_fix').'follower-notification');

    return $data;
}


public function findByAll($pagination = false, $perPage = 10, array $input = [] )
{

    $this->builder = $this->model->query();

    foreach ($input as $key => $value)
    {
        if (in_array($key, $this->model->searchables)) {
            is_string($input[$key]) ? $this->builder->where($key, 'like', '%' . $input[$key] . '%') : $this->builder->where($key, '=', $input[$key]);
        }
    }


    if (!empty($input['keyword'])) {
        $this->builder = $this->builder->join('users', 'users.id', 'followers.follow_id')
        ->where('users.first_name', 'like', '%'.$input['keyword'].'%');

    }

    $this->builder->select('followers.id');


    return parent::findByAll($pagination , $perPage, $input); 

}



public function findCollectionByCriteria($criteria = [] , $count = false, $orCrtieria = [], $startDate = null, $endDate = null)
{
    $this->builder = $this->model->newInstance();

    if($criteria) {
        $this->builder = $this->builder->where($criteria);
    }

// or Criteria must be an array 
    if($criteria && $orCrtieria) {
        foreach ($orCrtieria as $key => $where) {
            $this->builder  = $this->builder->orWhere(function ($query) use ($where) {
                $query->where($where);
            });
        }
    }


    if($startDate && $endDate) {
        $this->builder = $this->builder->whereBetween('created_at', [$startDate, $endDate]);
    }
    if($count){
        return  $this->builder->count();

    }

    return  $this->builder->get();

}

public function update(array $data = [])
{
    $data['requested'] = 0;

    if(!$data['follow']){

        $data['deleted_at'] = Carbon::now()->toDateTimeString();

    }

    unset($data['follow']);

    return parent::update($data);

}

    /**
     *
     * This method will fetch random model
     * and will return output back to client as json
     *
     * @access public
     * @return mixed
     *
     * @author Usaama Effendi <usaamaeffendi@gmail.com>
     *
     **/
    public function findAttributesOfCollectionByCriteria($criteria, $refresh = false, $details) {
        
        $attributes = $details['attributes'];

        $models = $this->model->newInstance()->where($criteria)->get($attributes);

        return $models;
        
    }


    
    public function findByCriteria($criteria, $refresh = false, $details = false, $encode = true) {
        $model = $this->model->newInstance()
                        ->where($criteria)->first(['id']);

        if ($model != NULL) {
            $model = $this->findById($model->id, $refresh, $details, $encode);
        }
        return $model;
    }



}
