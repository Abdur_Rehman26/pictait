<?php

namespace App\Data\Repositories;

use Kazmi\Data\Contracts\RepositoryContract;
use Kazmi\Data\Repositories\AbstractRepository;
use App\Data\Models\Post;
use Kazmi\Helpers\Helper;
use App\Jobs\LikeCreatedJob;
use \Cache;
use Carbon\Carbon;


class PostRepository extends AbstractRepository implements RepositoryContract
{
/**
*
* These will hold the instance of Post Class.
*
* @var object
* @access public
*
**/
public $model;
CONST PAGIATION = true , PER_PAGE = 5;
/**
*
* This is the prefix of the cache key to which the
* App\Data\Repositories data will be stored
* App\Data\Repositories Auto incremented Id will be append to it
*
* Example: Post-1
*
* @var string
* @access protected
*
**/

protected $_cacheKey = 'Post';
protected $_cacheTotalKey = 'total-Post';

public function __construct(Post $model)
{
    $this->model = $model;
    $this->builder = $model;

}

/**
*
* This method will fetch single model
* and will return output back to client as json
*
* @access public
* @return mixed
*
* @author Usaama Effendi <usaamaeffendi@gmail.com>
*
**/
public function findById($id, $refresh = false, $details = false, $encode = true) {

    $data = parent::findById($id, $refresh, $details, $encode);
    if($data){

        $path = config('uploads.images.user.public_relative').'/'.$data->user_id.'/posts/';

        $data->file_path = $data->file_name;

        if(substr($data->file_name, 0, 8) != "https://" && substr($data->file_name, 0, 8) != "http://"){
            
            if(!empty($data->ext)){
                $data->file_path = url($path.$data->file_name.'@2x.'.$data->ext);
            }    
        
        }

        $data->formatted_created_at = Carbon::parse($data->created_at)->diffForHumans();
        $data->user = app('UserRepository')->findById($data->user_id);

        $criteria = ['post_id' => $data->_id];

        $perPage = self::PER_PAGE;

        $perPage = request()->method_type == 'show' ? $perPage*3 : $perPage;

        $data->comments = app('CommentRepository')->findCollectionByCriteria(self::PAGIATION , self::PER_PAGE , $criteria);

        $data->commentsCount = app('CommentRepository')->findTotalCommentByInstance($data->_id , $refresh);

        $data->liked = false;

        if(!empty($data->likes)){
            $data->liked = in_array(request()->user()->id , $data->likes);            

            $i = 0;
            $limit = 3;
            $count = count($data->likes);

            while ($i < $limit && $i < $count) {
                $data->userLikes[] = app('UserRepository')->findById($data->likes[$i]);
                ++$i;
            }


        }

        return $data;
    }
    return null;
}


public function updateLikes($input)
{
    if(empty($input['liked'])){
        if($this->model->where('_id' , '=' , $input['post_id'])->whereNotIn('likes' , [$input['user_id']])->push('likes' , $input['user_id'])){

            $this->model->where('_id' , '=' , $input['post_id'])
            ->increment('likeCount');

            LikeCreatedJob::dispatch($input)->onQueue(config('queue.pre_fix').'like-notification');


        }

    }else{

        if($this->model->where('_id' , '=' , $input['post_id'])->whereIn('likes' , [$input['user_id']])->pull('likes' , $input['user_id'])){

            $this->model->where('_id' , '=' , $input['post_id'])
            ->decrement('likeCount');

        }


    }

    return $this->findById($input['post_id'], true);

}


public function findByAll($pagination = true, $perPage = 10, array $input = [] ) {

    $ids = $this->builder->orderBy('created_at' , 'DESC');

    if(!empty($input['user_id'])){
        $ids = $this->builder->where('user_id' , (int) $input['user_id']);
    }


    if ($pagination == true) {

        $ids = $ids->paginate($perPage);
        $models = $ids->items();

    } else {
        $ids = $ids->all();
        $models = $ids;
    }

    $data = ['data'=>[]];
    if ($models) {
        foreach ($models as &$model) {
            $model = $this->findById($model->id);
            if ($model) {
                $data['data'][] = $model;
            }
        }
    } 
    if ($pagination == true) {
// call method to paginate records
        $data = Helper::customPagination($data, $ids);
    }
    return $data;
}



public function findCollectionByCriteria($criteria = [] , $count = false, $orCrtieria = [], $startDate = null, $endDate = null)
{
    $this->builder = $this->model->newInstance();

    if($criteria) {
        $this->builder = $this->builder->where($criteria);
    }

// or Criteria must be an array 
    if($criteria && $orCrtieria) {
        foreach ($orCrtieria as $key => $where) {
            $this->builder  = $this->builder->orWhere(function ($query) use ($where) {
                $query->where($where);
            });
        }
    }


    if($startDate && $endDate) {
        $this->builder = $this->builder->whereBetween('created_at', [$startDate, $endDate]);
    }
    if($count){
        return  $this->builder->count();

    }

    return  $this->builder->get();

}

    /**
     *
     * This method will remove model
     * and will return output back to client as json
     *
     * @access public
     * @return bool
     *
     * @author Usaama Effendi <usaamaeffendi@gmail.com>
     *
     **/
    public function deleteById($id) {
        $criteria = ['post_id' => $id];

        app('CommentRepository')->deleteByCriteria($criteria);
        
        $criteria['type'] = 'post';
        app('FeedRepository')->deleteByCriteria($criteria);

        return parent::deleteById($id);
    }


}
