<?php

namespace App\Data\Repositories;

use Kazmi\Data\Contracts\RepositoryContract;
use Kazmi\Data\Repositories\AbstractRepository;
use App\Data\Models\RoleUser;

class RoleUserRepository extends AbstractRepository implements RepositoryContract
{
    /**
     *
     * These will hold the instance of RoleUser Class.
     *
     * @var object
     * @access public
     *
     **/
    public $model;

    /**
     *
     * This is the prefix of the cache key to which the
     * App\Data\Repositories data will be stored
     * App\Data\Repositories Auto incremented Id will be append to it
     *
     * Example: RoleUser-1
     *
     * @var string
     * @access protected
     *
     **/

    protected $_cacheKey = 'RoleUser';
    protected $_cacheTotalKey = 'total-RoleUser';

    public function __construct(RoleUser $model)
    {
        $this->model = $model;
        $this->builder = $model;

    }
}
