<?php

namespace App\Data\Repositories;

use Kazmi\Data\Contracts\RepositoryContract;
use Kazmi\Data\Repositories\AbstractRepository;
use App\Data\Models\User;
use Carbon\Carbon;
use DB;

class UserRepository extends AbstractRepository implements RepositoryContract
{


   CONST PER_PAGE = 10;
   CONST PAGINATION = true;
/**
     *
     * These will hold the instance of User Class.
     *
     * @var object
     * @access public
     *
     **/
public $model;

    /**
     *
     * This is the prefix of the cache key to which the
     * App\Data\Repositories data will be stored
     * App\Data\Repositories Auto incremented Id will be append to it
     *
     * Example: User-1
     *
     * @var string
     * @access protected
     *
     **/

    protected $_cacheKey = 'User';
    protected $_cacheTotalKey = 'total-User';

    public function __construct(User $model)
    {
        $this->model = $model;
        $this->builder = $model;

    }



    /**
     *
     * This method will fetch single model
     * and will return output back to client as json
     *
     * @access public
     * @return mixed
     *
     * @author Usaama Effendi <usaamaeffendi@gmail.com>
     *
     **/
    public function findById($id, $refresh = false, $details = false, $encode = true) {

        $data = parent::findById($id, $refresh, $details, $encode);
        if($data){

            $data->file_path = $data->image;

            if($data->image){
                if(substr($data->image, 0, 8) != "https://" && substr($data->image, 0, 8) != "http://"){

                    $data->file_path = userImagePath($data->image, $data->id);
                }
            }



            $data->formatted_created_at = Carbon::parse($data->created_at)->diffForHumans();


            return $data;
        }
        return null;
    }

    public function findCollectionByCriteria($pagination, $per_page, $input)
    {
        $this->builder = $this->model->where('id' , '!=' , $input['id'])->orderBy('id' , 'desc');

        // not searching last name

        if(!empty($input['keyword'])){
            $this->builder = $this->builder->where(function ($query) use ($input) {
                $query->where(DB::raw('concat(first_name," ", "")'), 'LIKE', "%{$input['keyword']}%");
                $query->orWhere('email', 'LIKE', "%{$input['keyword']}%");

            });
        }

        return parent::findByAll($pagination , $per_page);
    }


    public function searchFollowersByUser($input)
    {
        $this->builder = $this->model->newInstance();

        if (!empty($input['keyword'])) {
            $this->builder = $this->builder->join('followers', 'users.id', 'followers.follow_id')
            ->where('users.first_name', 'like', '%'.$input['keyword'].'%')
            ->where('followers.user_id' , $input['user_id']);
        }

        $this->builder->select('users.*');

        return parent::findByAll(self::PAGINATION, self::PER_PAGE);

    }


    public function searchCriteria($criteria)
    {

        foreach ($criteria as $key => $value)
        {
            if (in_array($key, $this->model->searchables)) {

                if(!$criteria[$key]){

                    $this->builder = $this->builder->whereNull($key);

                }else{

                    $this->builder = is_string($criteria[$key]) ? $this->builder->where($key, 'like', '%' . $criteria[$key] . '%') : $this->builder->where($key, '=', $criteria[$key]);

                }

            }
        }
        return $this->builder;
    }


}
