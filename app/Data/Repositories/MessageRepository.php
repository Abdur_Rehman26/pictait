<?php

namespace App\Data\Repositories;

use Kazmi\Data\Contracts\RepositoryContract;
use Kazmi\Data\Repositories\AbstractRepository;
use App\Data\Models\Message;
use Kazmi\Helpers\Helper;
use Carbon\Carbon;

class MessageRepository extends AbstractRepository implements RepositoryContract
{
/**
     *
     * These will hold the instance of Message Class.
     *
     * @var object
     * @access public
     *
     **/
public $model;

    /**
     *
     * This is the prefix of the cache key to which the
     * App\Data\Repositories data will be stored
     * App\Data\Repositories Auto incremented Id will be append to it
     *
     * Example: Message-1
     *
     * @var string
     * @access protected
     *
     **/

    protected $_cacheKey = 'Message';
    protected $_cacheTotalKey = 'total-Message';

    public function __construct(Message $model)
    {
        $this->model = $model;
        $this->builder = $model;

    }


    public function findByUserId($user_id, $recipient_id)
    {
        $members = [$user_id , $recipient_id];
        $messages = $this->model->where('members' , 'all', $members)->get();
        
        if ($messages->count() > 0){
            return $messages->first();
        }

        $input =  ['type' => 'user' , 'members' => [ $recipient_id, $user_id] , 'recipients' => [$recipient_id]];
        $input['created_by'] = $user_id;

        return parent::create($input);

    }

    public function findByAll($pagination = true, $perPage = 10, array $input = [] ) {

        $ids = $this->builder->orderBy('created_at' , 'DESC');

        if(!empty($input['user_id'])){
            $ids = $this->builder->whereIn('members' , [(int) $input['user_id']]);
        }

        if ($pagination == true) {

            $ids = $ids->paginate($perPage);
            $models = $ids->items();

        } else {
            $ids = $ids->all();
            $models = $ids;
        }

        $data = ['data'=>[]];
        if ($models) {
            foreach ($models as &$model) {
                $model = $this->findById($model->id);
                if ($model) {
                    $data['data'][] = $model;
                }
            }
        } 
        if ($pagination == true) {
            // call method to paginate records
            $data = Helper::customPagination($data, $ids);
        }
        return $data;
    }


    /**
     *
     * This method will fetch single model
     * and will return output back to client as json
     *
     * @access public
     * @return mixed
     *
     * @author Usaama Effendi <usaamaeffendi@gmail.com>
     *
     **/
    public function findById($id, $refresh = false, $details = false, $encode = true) {

        $data = parent::findById($id, $refresh, $details, $encode);
        if($data){
            $data->last_message = app('MessageResponseRepository')->findByAttribute('message_id' , $data->_id);
            $data->formatted_created_at = Carbon::parse($data->created_at)->diffForHumans();
            $data->user = app('UserRepository')->findById($data->created_by);

            if(request()->user()){
                
                $recipient_id = array_diff($data->members, [request()->user()->id]);
                $recipient_id = reset($recipient_id);

                $data->recipient_user = app('UserRepository')->findById($recipient_id);
            }

            return $data;
        }
        return null;
    }


    public function findByCriteria($criteria, $refresh = false, $details = false, $encode = true) {
        $model = $this->model->newInstance()
                        ->where($criteria)->first(['id']);

        if ($model != NULL) {
            $model = $this->findById($model->id, $refresh, $details, $encode);
        }
        return $model;
    }

    
}
