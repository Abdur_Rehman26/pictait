<?php

namespace App\Data\Repositories;

use Kazmi\Data\Contracts\RepositoryContract;
use Kazmi\Data\Repositories\AbstractRepository;
use App\Data\Models\Hall;

class HallRepository extends AbstractRepository implements RepositoryContract
{
    /**
     *
     * These will hold the instance of Hall Class.
     *
     * @var object
     * @access public
     *
     **/
    public $model;

    /**
     *
     * This is the prefix of the cache key to which the
     * App\Data\Repositories data will be stored
     * App\Data\Repositories Auto incremented Id will be append to it
     *
     * Example: Hall-1
     *
     * @var string
     * @access protected
     *
     **/

    protected $_cacheKey = 'Hall';
    protected $_cacheTotalKey = 'total-Hall';

    public function __construct(Hall $model)
    {
        $this->model = $model;
        $this->builder = $model;

    }
}
