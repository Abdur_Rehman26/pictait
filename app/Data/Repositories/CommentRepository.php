<?php

namespace App\Data\Repositories;

use Kazmi\Data\Contracts\RepositoryContract;
use Kazmi\Data\Repositories\AbstractRepository;
use App\Data\Models\Comment;
use Kazmi\Helpers\Helper;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;

class CommentRepository extends AbstractRepository implements RepositoryContract
{
/**
     *
     * These will hold the instance of Comment Class.
     *
     * @var object
     * @access public
     *
     **/
    public $model;

    /**
     *
     * This is the prefix of the cache key to which the
     * App\Data\Repositories data will be stored
     * App\Data\Repositories Auto incremented Id will be append to it
     *
     * Example: Comment-1
     *
     * @var string
     * @access protected
     *
     **/

    protected $_cacheKey = 'Comment';
    protected $_cacheTotalKey = 'total-Comment';

    public function __construct(Comment $model)
    {
        $this->model = $model;
        $this->builder = $model;

    }

    public function findCollectionByCriteria($pagination = false, $perPage = 10, array $input = [])
    {
        $criteria = ['post_id' => $input['post_id']];
        $ids = $this->builder->where($criteria)->orderBy('updated_at', 'desc');
     
        if ($pagination == true) {

            $ids = $ids->paginate($perPage);
            $models = $ids->items();

        } else {
            $ids = $ids->all();
            $models = $ids;
        }

        $data = ['data'=>[]];
        if ($models) {
            foreach ($models as &$model) {
                $model = $this->findById($model->id);
                if ($model) {
                    $data['data'][] = $model;
                }
            }
        }
        if ($pagination == true) {
            // call method to paginate records
            $data = Helper::customPagination($data, $ids);
        }
        return $data;
    }



        public function findByAll($pagination = true, $perPage = 10, array $input = [] ) {
        $ids = $this->builder;
        
        if ($pagination == true) {

            $ids = $ids->paginate($perPage);
            $models = $ids->items();

        } else {
            $ids = $ids->all();
            $models = $ids;
        }

        $data = ['data'=>[]];
        if ($models) {
            foreach ($models as &$model) {
                $model = $this->findById($model->id);
                if ($model) {
                    $data['data'][] = $model;
                }
            }
        } 
        if ($pagination == true) {
            // call method to paginate records
            $data = Helper::customPagination($data, $ids);
        }
        return $data;
    }



        /**
     *
     * This method will fetch single model
     * and will return output back to client as json
     *
     * @access public
     * @return mixed
     *
     * @author Usaama Effendi <usaamaeffendi@gmail.com>
     *
     **/
    public function findById($id, $refresh = false, $details = false, $encode = true) {
        
        $data = parent::findById($id, $refresh, $details, $encode);
        if($data){

        $data->user = app('UserRepository')->findById($data->user_id);
        $data->formatted_created_at = Carbon::parse($data->created_at)->diffForHumans();

        return $data;
        }
        return null;
    }

    public function findTotalCommentByInstance($id , $refresh = false)
    {

        $cacheKey = $this->_cacheTotalKey."-".$id;
        $total = Cache::get($cacheKey);
        if ($total == NULL || $refresh == true) {
            $total = $this->model->where('post_id' , $id)->count();
            Cache::forever($cacheKey, $total);
        }
        return $total;
    }


    public function create(array $data = [])
    {
        $data = parent::create($data);
    
        return app('PostRepository')->findById($data->post_id , true);
    }

    public function deleteByCriteria($criteria)
    {
        $models = $this->model->where($criteria);
    
        $models->delete();

        foreach ($models as $key => $model) {
            parent::findById($model->id, true);
        }

        return true;
    }


}
