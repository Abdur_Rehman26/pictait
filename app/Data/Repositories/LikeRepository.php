<?php

namespace App\Data\Repositories;

use Kazmi\Data\Contracts\RepositoryContract;
use Kazmi\Data\Repositories\AbstractRepository;
use App\Data\Models\Like;

class LikeRepository extends AbstractRepository implements RepositoryContract
{
/**
     *
     * These will hold the instance of Like Class.
     *
     * @var object
     * @access public
     *
     **/
    public $model;

    /**
     *
     * This is the prefix of the cache key to which the
     * App\Data\Repositories data will be stored
     * App\Data\Repositories Auto incremented Id will be append to it
     *
     * Example: Like-1
     *
     * @var string
     * @access protected
     *
     **/

    protected $_cacheKey = 'Like';
    protected $_cacheTotalKey = 'total-Like';

    public function __construct(Like $model)
    {
        $this->model = $model;
        $this->builder = $model;

    }
}
