<?php

namespace App\Data\Repositories;

use Kazmi\Data\Contracts\RepositoryContract;
use Kazmi\Data\Repositories\AbstractRepository;
use App\Data\Models\Feed;
use Kazmi\Helpers\Helper;
use Carbon\Carbon;

class FeedRepository extends AbstractRepository implements RepositoryContract
{
/**
     *
     * These will hold the instance of Feed Class.
     *
     * @var object
     * @access public
     *
     **/
    public $model;

    /**
     *
     * This is the prefix of the cache key to which the
     * App\Data\Repositories data will be stored
     * App\Data\Repositories Auto incremented Id will be append to it
     *
     * Example: Feed-1
     *
     * @var string
     * @access protected
     *
     **/

    protected $_cacheKey = 'Feed';
    protected $_cacheTotalKey = 'total-Feed';

    public function __construct(Feed $model)
    {
        $this->model = $model;
        $this->builder = $model;

    }

    public function composeData($input)
    {
        $data = ['user_id' => $input->user_id , 'type' => 'post', 'posted_by' => $input->posted_by, 'post_id' => $input->_id, 'posted_by_id' => $input->posted_by_id, 'text_template' => 'posted by :'];

        return $data;

    }

    public function findByAll($pagination = true, $perPage = 10, array $input = [] ) {
        
        $criteria = ['user_id' => $input['user_id'] , 'type' => 'user' , 'deleted_at' => null , 'requested' => false];
        $details = ['attributes' => ['follow_id']];

        $followingIds = app('FollowerRepository')->findAttributesOfCollectionByCriteria($criteria , false, $details);
        
        $followingIds = $followingIds->pluck('follow_id')->all();

        $followingIds[] = $input['user_id'];
        
        $ids = $this->builder->orderBy('updated_at' , 'DESC')->whereIn('user_id' , $followingIds);
        
        if ($pagination == true) {

            $ids = $ids->paginate($perPage);
            $models = $ids->items();

        } else {
            $ids = $ids->all();
            $models = $ids;
        }

        $data = ['data'=>[]];
        if ($models) {
            foreach ($models as &$model) {
                $model = $this->findById($model->id);
                if ($model) {
                    $data['data'][] = $model;
                }
            }
        } 
        if ($pagination == true) {
            // call method to paginate records
            $data = Helper::customPagination($data, $ids);
        }
        return $data;
    }

    /**
     *
     * This method will fetch single model
     * and will return output back to client as json
     *
     * @access public
     * @return mixed
     *
     * @author Usaama Effendi <usaamaeffendi@gmail.com>
     *
     **/
    public function findById($id, $refresh = false, $details = false, $encode = true) {
        
        $data = parent::findById($id, $refresh, $details, $encode);
        
        $data->user = app('UserRepository')->findById($data->user_id);
        $data->post = app('PostRepository')->findById($data->post_id);

        $data->formatted_created_at = Carbon::parse($data->created_at)->diffForHumans();
        return $data;
    }


    public function deleteByCriteria($criteria)
    {
        $models = $this->model->where($criteria);
    
        $models->delete();

        foreach ($models as $key => $model) {
            parent::findById($model->id, true);
        }

        return true;
    }


}
