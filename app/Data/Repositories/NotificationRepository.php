<?php

namespace App\Data\Repositories;

use Kazmi\Data\Contracts\RepositoryContract;
use Kazmi\Data\Repositories\AbstractRepository;
use App\Data\Models\Notification;
use Carbon\Carbon;

class NotificationRepository extends AbstractRepository implements RepositoryContract
{
/**
*
* These will hold the instance of Notification Class.
*
* @var object
* @access public
*
**/
public $model;

/**
*
* This is the prefix of the cache key to which the
* App\Data\Repositories data will be stored
* App\Data\Repositories Auto incremented Id will be append to it
*
* Example: Notification-1
*
* @var string
* @access protected
*
**/

protected $_cacheKey = 'Notification';
protected $_cacheTotalKey = 'total-Notification';

public function __construct(Notification $model)
{
    $this->model = $model;
    $this->builder = $model;

}

public function findCollectionByCriteria($criteria = [] , $count = false, $orCrtieria = [], $startDate = null, $endDate = null)
{
    $this->builder = $this->model->newInstance();


    $this->builder = self::searchCriteria($criteria);
// $or Criteria must be an array 

    if($criteria && $orCrtieria) {
        foreach ($orCrtieria as $key => $where) {
            $this->builder  = $this->builder->orWhere(function ($query) use ($where) {
                $query->where($where);
            });
        }
    }

    if($startDate && $endDate) {
        $this->builder = $this->builder->whereBetween('created_at', [$startDate, $endDate]);
    }
    if($count){
        return  $this->builder->count();
    }

    return  $this->builder->get();

}

/**
*
* This method will fetch all exsiting models
* and will return output back to client as json
*
* @access public
* @return mixed
*
* @author Usaama Effendi <usaamaeffendi@gmail.com>
*
**/
public function findByAll($pagination = false, $perPage = 10, array $input = [] ) {

    $this->builder = $this->model->orderBy('created_at' , 'desc');

    unset($input['pagination']);

    $this->builder = self::searchCriteria($input);

    $data = parent::findByAll($pagination, $perPage, $input);
    return $data;
}


/**
*
* This method will fetch single model
* and will return output back to client as json
*
* @access public
* @return mixed
*
* @author Usaama Effendi <usaamaeffendi@gmail.com>
*
**/
public function findById($id, $refresh = false, $details = false, $encode = true) {
    $data =  parent::findById($id, $refresh, $details, $encode);

    if($data){

        $notificationBody = $data->data;
        $data->from = app('UserRepository')->findById($notificationBody['from']['id']);
        

        $data->action = !empty($notificationBody['action']) ? $notificationBody['action'] : ''; 
        $notificationType = notificationTypeAndText($data);
        $data->type = $notificationType['type'];

        $data->notifying_object = self::getNotifyingObject($notificationBody, $data->type); 

        $data->text = $notificationType['text'];
        $data->notifier_image = $data->from->file_path; 
        $data->notifier = $data->from; 
        $data->notifier_name = $data->from->first_name;


        $data->formatted_created_at = Carbon::parse($data->created_at)->diffForHumans();;
    }

    return $data;

}

function bulkUpdate($input)
{
    $this->builder = $this->model;

    $this->builder =self::searchCriteria($input);

    $unReadNotifications = $this->builder->get();

    foreach ($unReadNotifications as $key => $unReadNotification) {

        $updateData = ['id' => $unReadNotification['id'] , 'read_at' =>  Carbon::now()->toDateTimeString()];

        parent::update($updateData);
    }

    return true;
}


public function searchCriteria($criteria)
{

    foreach ($criteria as $key => $value)
    {
        if (in_array($key, $this->model->searchables)) {

            if($key == 'type'){


                $this->builder = $this->builder->where(function ($query) use ($criteria, $key)
                {
                    foreach ($this->model->notificationTypes[$criteria[$key]] as $notificationKey => $notificationValue) {
                        $query->orWhere('type' , $notificationValue);
                    }
                });


            }elseif(!$criteria[$key]){

                $this->builder = $this->builder->whereNull($key);

            }else{

                $this->builder = is_string($criteria[$key]) ? $this->builder->where($key, 'like', '%' . $criteria[$key] . '%') : $this->builder->where($key, '=', $criteria[$key]);

            }

        }
    }
    return $this->builder;
}

public function getNotifyingObject($object, $type)
{
    $object = !empty($object['notifying_object']) ? $object['notifying_object'] : null;
    if($type == 'dare'){
        return app('DareRepository')->findById($object['_id']);
    }elseif($type == 'follower'){
        return app('FollowerRepository')->findById($object['id']);
    }elseif($type == 'post'){
        return app('PostRepository')->findById($object['_id']);
    }elseif($type == 'event'){
            return $object;
    }else{

    }

}

}
