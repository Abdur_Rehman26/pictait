<?php

namespace App\Data\Repositories;

use Kazmi\Data\Contracts\RepositoryContract;
use Kazmi\Data\Repositories\AbstractRepository;
use App\Data\Models\Event;
use Carbon\Carbon;
use App\Http\Traits\AbstractMethods;

class EventRepository extends AbstractRepository implements RepositoryContract
{

  use AbstractMethods;
    /**
    *
    * These will hold the instance of Event Class.
    *
    * @var object
    * @access public
    *
    **/
    public $model;

    /**
    *
    * This is the prefix of the cache key to which the
    * App\Data\Repositories data will be stored
    * App\Data\Repositories Auto incremented Id will be append to it
    *
    * Example: Event-1
    *
    * @var string
    * @access protected
    *
    **/

    protected $_cacheKey = 'Event';
    protected $_cacheTotalKey = 'total-Event';

    public function __construct(Event $model)
    {
      $this->model = $model;
      $this->builder = $model;

    }

    public function findById($id, $refresh = false, $details = false, $encode = true) {

      $data = parent::findById($id, $refresh);

      if($data){


        $criteria = ['user_id' => request()->user()->id , 'follow_id' =>  (int) $id , 'type' => 'event' , 'deleted_at' => null];

        $data->isFollowing = app('FollowerRepository')->findByCriteria($criteria) ? true : false;


        $data->formatted_created_at = Carbon::parse($data->created_at)->diffForHumans();

        $data->displayImage = $data->image;

        if($data->image){
          if(substr($data->image, 0, 8) != "https://" && substr($data->image, 0, 8) != "http://"){

            $data->displayImage = userImagePath($data->image , $data->id, 'event');
          }
        }
        $criteria = ['user_id' => request()->user()->id , 'event_id' => $data->id ];
        $data->userEntry = app('EventEntryRepository')->findByCriteria($criteria);

        $criteria = ['status' => 1 , 'event_id' => $data->id ];
        $data->uploadedImagesCount = app('EventEntryRepository')->findCountByCriteria($criteria);

        $criteria = ['type' => 'event', 'follow_id' => $data->id];

        $followers = app('FollowerRepository')->findByAll(true, 5, $criteria);

        $data->followers = $followers['data'];

      }
      return $data;
    }



    public function findByAll($pagination = false, $perPage = 10, array $input = [] )
    {

      $this->builder = $this->model->orderBy('created_at', 'desc');

      $this->builder = $this->searchCriteria($input);

      return parent::findByAll($pagination , $perPage, $input);

    }


  }


