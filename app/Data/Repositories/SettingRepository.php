<?php

namespace App\Data\Repositories;

use Kazmi\Data\Repositories\AbstractRepository;
use Kazmi\Data\Contracts\RepositoryContract;
use Illuminate\Support\Facades\Cache;
use App\Data\Models\Setting;
use Carbon\Carbon;

class SettingRepository extends AbstractRepository implements RepositoryContract
{
/**
     *
     * These will hold the instance of Setting Class.
     *
     * @var object
     * @access public
     *
     **/
    public $model;

    /**
     *
     * This is the prefix of the cache key to which the
     * App\Data\Repositories data will be stored
     * App\Data\Repositories Auto incremented Id will be append to it
     *
     * Example: Setting-1
     *
     * @var string
     * @access protected
     *
     **/

    protected $_cacheKey = 'Setting';
    protected $_cacheTotalKey = 'total-Setting';

    public function __construct(Setting $model)
    {
        $this->model = $model;
        $this->builder = $model;

    }


    public function create(array $data = [])
    {

        $user_id = request()->user_id;
        if($updatedData = Setting::where('user_id' , $user_id)->first()){
            $updatedData->friend_requests = $data['friend_requests'];
            $updatedData->posts = $data['posts'];
            $updatedData->updated_at = Carbon::now();
            $updatedData->save();
            return self::findByAttribute('user_id' , $user_id , true);

        }else{
        if ($updatedData = Setting::create($data)) {

            return $updatedData; 
        }            
        }
        return false;
    }

}
