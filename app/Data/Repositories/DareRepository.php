<?php

namespace App\Data\Repositories;

use Kazmi\Data\Contracts\RepositoryContract;
use Kazmi\Data\Repositories\AbstractRepository;
use App\Data\Models\Dare;
use App\Jobs\DareLikedJob;
use Carbon\Carbon;

class DareRepository extends AbstractRepository implements RepositoryContract
{
/**
*
* These will hold the instance of Dare Class.
*
* @var object
* @access public
*
**/
public $model;

CONST PAGINATION = true;
CONST PER_PAGE = 20;

/**
*
* This is the prefix of the cache key to which the
* App\Data\Repositories data will be stored
* App\Data\Repositories Auto incremented Id will be append to it
*
* Example: Dare-1
*
* @var string
* @access protected
*
**/

protected $_cacheKey = 'Dare';
protected $_cacheTotalKey = 'total-Dare';

public function __construct(Dare $model)
{
    $this->model = $model;
    $this->builder = $model;

}

public function findById($id, $refresh = false, $details = false, $encode = true) {

    $data = parent::findById($id, $refresh);

    if($data){

        if(!empty($data->userImage)){

            $data->userImagePath = userImagePath($data->userImage , $data->user_id, 'dare');

        }

        if(!empty($data->challengerImage)){

            $data->challengerImagePath = userImagePath($data->challengerImage , $data->challenger_id, 'dare');

        }        


        $data->user = app('UserRepository')->findById($data->user_id);
        $data->challenger = app('UserRepository')->findById($data->challenger_id);



        $data->userLikedByMe = false;
        $data->challengerLikedByMe = false;

        if(!empty($data->userLikes)){
            $data->userLikedByMe = in_array(request()->user()->id , $data->userLikes);
        }

        if(!empty($data->challengerLikes)){
            $data->challengerLikedByMe = in_array(request()->user()->id , $data->challengerLikes);
        }

        $data->formatted_created_at = Carbon::parse($data->created_at)->diffForHumans();

    }
    return $data;
}


public function findCollectionByCriteria($criteria = [] , $count = false, $orCrtieria = [], $startDate = null, $endDate = null)
{
    $this->builder = $this->model->newInstance();

    if($criteria) {
        $this->builder = $this->builder->where($criteria);
    }

// $or Criteria must be an array 

    if($criteria && $orCrtieria) {
        foreach ($orCrtieria as $key => $where) {
            $this->builder  = $this->builder->orWhere(function ($query) use ($where) {
                $query->where($where);
            });
        }
    }

    if($startDate && $endDate) {
        $this->builder = $this->builder->whereBetween('created_at', [$startDate, $endDate]);
    }
    if($count){
        return  $this->builder->count();
    }

    return  $this->builder->get();

}


public function findByAll($pagination = false, $perPage = 10, array $input = [] )
{

    $this->builder = $this->model->query()->orderBy('created_at', 'desc');
    
    if(!empty($input['show_all'])){
        unset($input['user_id']);
    }
    

    foreach ($input as $key => $value)
    {

        if (in_array($key, $this->model->searchables)) {

            if($key == 'status'){
                $this->builder->where($key, '=', $input[$key]);

                if($input[$key] != 'pending'){
                    $this->builder->whereNotNull('challengerImage');
                }
            }elseif($key == 'pin'){

                $this->builder->whereIn('pinToProfile' , [$input['user_id']]);

            }elseif($key == 'user_id'){
                $this->builder->where('challenger_id' , $value)
                ->orWhere('user_id' , $value);

            }else{

                is_string($input[$key]) ? $this->builder->where($key, 'like', '%' . $input[$key] . '%') : $this->builder->where($key, '=', $input[$key]);
            }


        }
    }

    $perPage = (int) $perPage;

    return parent::findByAll($pagination , $perPage, $input); 

}


public function dareLike(array $data = [])
{
    if($data['type'] == 'user'){

        if($data['liked']){

            if($this->model->where('_id' , '=' , $data['id'])->whereIn('userLikes' , [$data['user_id']])->pull('userLikes' , $data['user_id'])){

                $this->model->where('_id' , '=' , $data['id'])
                ->decrement('userLikesCount');
            }

        }else{

            if($this->model->where('_id' , '=' , $data['id'])->whereNotIn('userLikes' , [$data['user_id']])->push('userLikes' , $data['user_id'])){

                $this->model->where('_id' , '=' , $data['id'])
                ->increment('userLikesCount');


                DareLikedJob::dispatch($data)->onQueue(config('queue.pre_fix').'dare-like-notification');
            }

        }


    }else{

        if($data['liked']){

            if($this->model->where('_id' , '=' , $data['id'])->whereIn('challengerLikes' , [$data['user_id']])->pull('challengerLikes' , $data['user_id'])){

                $this->model->where('_id' , '=' , $data['id'])
                ->decrement('challengerLikesCount');
            }

        }else{

            if($this->model->where('_id' , '=' , $data['id'])->whereNotIn('challengerLikes' , [$data['user_id']])->push('challengerLikes' , $data['user_id'])){

                $this->model->where('_id' , '=' , $data['id'])
                ->increment('challengerLikesCount');

                DareLikedJob::dispatch($data)->onQueue(config('queue.pre_fix').'dare-like-notification');
            }

        }
    }

    return $this->findById($data['id'], true);

}

public function pinToProfile(array $data = [])
{

    $this->model->where('_id' , '=' , $data['id'])->whereNotIn('pinToProfile' , [$data['user_id']])->push('pinToProfile' , $data['user_id']);

    $this->model->where('_id' , '<>' , $data['id'])->where(function($query) use ($data){
        $query->where('user_id' , $data['user_id'])
        ->orWhere('challenger_id' , $data['user_id']);
    })->pull('pinToProfile' , $data['user_id']);

    $updateData = ['id' =>  $data['id']];

    return parent::update($updateData);

}

}
