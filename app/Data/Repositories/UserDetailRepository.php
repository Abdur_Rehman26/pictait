<?php

namespace App\Data\Repositories;

use Kazmi\Data\Contracts\RepositoryContract;
use Kazmi\Data\Repositories\AbstractRepository;
use Illuminate\Support\Facades\Cache;
use App\Data\Models\UserDetail;
use App\Data\Models\User;
use Carbon\Carbon;

class UserDetailRepository extends AbstractRepository implements RepositoryContract
{
/**
     *
     * These will hold the instance of UserDetail Class.
     *
     * @var object
     * @access public
     *
     **/
public $model;

    /**
     *
     * This is the prefix of the cache key to which the
     * App\Data\Repositories data will be stored
     * App\Data\Repositories Auto incremented Id will be append to it
     *
     * Example: UserDetail-1
     *
     * @var string
     * @access protected
     *
     **/

    protected $_cacheKey = 'UserDetail';
    protected $_cacheTotalKey = 'total-UserDetail';

    public function __construct(UserDetail $model)
    {
        $this->model = $model;
        $this->builder = $model;

    }


    public function getSingle($id , $refresh = false)
    {
        $data  = $this->findByAttribute('user_id' , (int) $id , true);

        if($data){
            $data->formatted_date_of_birth = Carbon::parse($data->date_of_birth)->format('jS \o\f F, Y');
        }else{
            $user = User::find($id);
            $data = [
                'first_name' => $user['first_name'],
                'last_name'=> $user['last_name'],
                'date_of_birth'=> $user['date_of_birth'],
                'user_id' => $user['id'],
                'country'=> $user['country'],
                'email'=> $user['email'],
                'status'=> $user['status'],
                'bio'=> $user['bio'],
                'gender'=> $user['gender'],
            ];
            $data = parent::create($data);
            return $data;
        }

        return $data;
    }

    public function create(array $data = [])
    {
        $user_id = request()->user_id;
        $data['user_id'] = $user_id;

        if($updatedData = UserDetail::where('user_id' , $user_id)->first()){
            $updatedData->first_name = $data['first_name'];
            $updatedData->last_name = $data['last_name'];
            $updatedData->date_of_birth = $data['date_of_birth'];
            $updatedData->country = $data['country'];
            $updatedData->email = $data['email'];
            $updatedData->status = $data['status'];
            $updatedData->bio = $data['bio'];
            $updatedData->user_id = $user_id;
            $updatedData->gender = $data['gender'];
            $updatedData->updated_at = Carbon::now();
            $updatedData->save();

            Cache::forever($this->_cacheKey.'-single-'.$user_id, $updatedData);

            return $updatedData;
        }else{
            if ($updatedData = UserDetail::create($data)) {
                return $updatedData;
            }
        }
        return false;
    }


}
