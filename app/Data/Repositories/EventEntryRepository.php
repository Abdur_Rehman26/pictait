<?php

namespace App\Data\Repositories;

use Kazmi\Data\Contracts\RepositoryContract;
use Kazmi\Data\Repositories\AbstractRepository;
use App\Data\Models\EventEntry;
use App\Http\Traits\AbstractMethods;
use Carbon\Carbon;

class EventEntryRepository extends AbstractRepository implements RepositoryContract
{
  use AbstractMethods;
    /**
     *
     * These will hold the instance of EventEntry Class.
     *
     * @var object
     * @access public
     *
     **/
    public $model;

    /**
     *
     * This is the prefix of the cache key to which the
     * App\Data\Repositories data will be stored
     * App\Data\Repositories Auto incremented Id will be append to it
     *
     * Example: EventEntry-1
     *
     * @var string
     * @access protected
     *
     **/

    protected $_cacheKey = 'event-entry';
    protected $_cacheTotalKey = 'total-event-entry';

    public function __construct(EventEntry $model)
    {
      $this->model = $model;
      $this->builder = $model;

    }

    public function findById($id, $refresh = false, $details = false, $encode = true) {

      $data = parent::findById($id, $refresh);

      if($data){

        $data->liked = false;

        if(!empty($data->userLikes)){
          $data->liked = in_array(request()->user()->id , $data->userLikes);            

          $i = 0;
          $limit = 3;
          $count = count($data->userLikes);

          while ($i < $limit && $i < $count) {
            if($data->userLikes[$i]){
              $data->userLikesData[] = app('UserRepository')->findById($data->userLikes[$i]);
            }
            ++$i;
          }

        }

      }
      return $data;
    }




    public function findByAll($pagination = false, $perPage = 10, array $input = [] )
    {
      $this->builder = $this->model->query()->orderBy('created_at', 'desc');  
      $this->builder = $this->searchCriteria($input);

      $perPage = (int) $perPage;


      return parent::findByAll($pagination , $perPage, $input); 

    }

    public function composeEntryData($input)
    {
      $data = [];
      $created_at = $updated_at = Carbon::now()->toDateTimeString();

      foreach ($input['entries'] as $key => $value) {
        $data[] = [
          'event_id' => $input['event_id'],
          'entry' => $value['image'],
          'created_at' => $created_at,
          'updated_at' => $updated_at,
          'user_id' => $input['user_id'],
          'status' => 1
        ];
      }
      return $data;
    }


    public function bulkInsert($data)
    {
      $this->model->insert($data);
      return true;
    }

    public function updateLike( array $data = [])
    {

      $likedPreviouslyData = [];
      $likedPreviouslyIds = [];

      if(!empty($data['liked'])){

        $this->model->where('_id' , '=' , $data['id'])->whereIn('userLikes' , [$data['user_id']])->pull('userLikes' , $data['user_id']);

      }else{

        $likedPreviously = $this->model->where('event_id', $data['event_id'])->where('_id' , '!=', $data['id'])->whereIn('userLikes', [$data['user_id']]);

        $likedPreviouslyIds = $likedPreviously->pluck('_id')->toArray();
        
        $likedPreviously->pull('userLikes', [$data['user_id']]);

        $this->model->where('_id' , '=' , $data['id'])->whereNotIn('userLikes' , [$data['user_id']])->push('userLikes' , $data['user_id']);

      }

      foreach ($likedPreviouslyIds as $key => $id) {
        $likedPreviouslyData[] = $this->findById($id, true);
      }


      $data = $this->findById($data['id'], true);
      $data->likedPreviouslyData = $likedPreviouslyData;

      return $data;

    }


    public function findByCriteria($criteria, $refresh = false, $details = false, $encode = true) {
      $model = $this->model->newInstance()
      ->where($criteria)->first(['id']);

      if ($model != NULL) {
        $model = $this->findById($model->id, $refresh, $details, $encode);
      }
      return $model;
    }

    public function findCountByCriteria($criteria, $details = false) {
      return $this->model->newInstance()
      ->where($criteria)->count();
    }



  }
