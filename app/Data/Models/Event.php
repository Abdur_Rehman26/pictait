<?php

namespace App\Data\Models;
use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
	public $searchables = [
		'user_id',
		'name'
	];

}
