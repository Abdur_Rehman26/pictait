<?php

namespace App\Data\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Event;

class Post extends Eloquent
{
	protected $connection = 'mongodb';
	protected $collection = 'posts';


   	protected $fillable = [
   		'friend_request',
   		'posts',
   	];    

}