<?php

namespace App\Data\Models;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Database\Eloquent\Model;
use Event;

class MessageResponse extends Eloquent 
{
    use SoftDeletes;

	protected $connection = 'mongodb';
	protected $collection = 'message_responses';

	protected $fillable = [
		'reply', 'is_seen', 'user_id', 'created_at', 'updated_at', 'deleted_at'
	];

	
	

}