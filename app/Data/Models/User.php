<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;
use Yadakhov\InsertOnDuplicateKey;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
	use InsertOnDuplicateKey, Notifiable, HasApiTokens;
    //


	public function setGenderAttribute($value){
		$value = 2;	
		if($value == 'male')
		{
			$value = 1;
		}
		$this->attributes['gender'] = $value;
	}

}
