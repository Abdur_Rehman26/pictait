<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class EventEntry extends Eloquent
{
	protected $connection = 'mongodb';
	protected $collection = 'event_entries';

	public $searchables = [
		'event_id',
		'status'
	];


}
