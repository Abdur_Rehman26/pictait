<?php

namespace App\Data\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Event;

class Comment extends Eloquent
{
	protected $connection = 'mongodb';
	protected $collection = 'comments';

}