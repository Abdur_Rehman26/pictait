<?php

namespace App\Data\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class UserDetail extends Eloquent
{
	protected $connection = 'mongodb';
	protected $collection = 'user_details';


   	protected $fillable = [
   		'first_name' , 'last_name' , 'email' , 'date_of_birth' , 'gender' , 'country' , 'status' , 'bio' , 'user_id'
   	];
}
