<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;
use Yadakhov\InsertOnDuplicateKey;
use Event;
use Illuminate\Database\Eloquent\SoftDeletes;

class Follower extends Model
{
	use InsertOnDuplicateKey,SoftDeletes;
	
	public $searchables = [
        'user_id',
        'follow_id',
        'requested',
        'type'
    ];


   

}