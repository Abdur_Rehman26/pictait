<?php

namespace App\Data\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Event;

class Dare extends Eloquent
{
	protected $connection = 'mongodb';
	protected $collection = 'dares';


	public $searchables = [
        'user_id',
        'challenger_id',
        'status',
        'pin'
    ];

  

}