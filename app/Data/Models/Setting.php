<?php

namespace App\Data\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Event;

class Setting extends Eloquent
{
	protected $connection = 'mongodb';
	protected $collection = 'settings';


   	protected $fillable = [
   		'friend_requests',
   		'posts',
   		'user_id'
   	];


}
