<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;
use Event;
use Illuminate\Database\Eloquent\SoftDeletes;

class Notification extends Model
{

    use SoftDeletes;
//

/**
* Indicates if the IDs are auto-incrementing.
*
* @var bool
*/
public $incrementing = false;

/**
* The table associated with the model.
*
* @var string
*/
protected $table = 'notifications';

/**
* The guarded attributes on the model.
*
* @var array
*/
protected $guarded = [];

/**
* The attributes that should be cast to native types.
*
* @var array
*/
protected $casts = [
    'data' => 'array',
    'read_at' => 'datetime',
];

protected $fillable = ['read_at'];

public $searchables = [
    'notifiable_id',
    'type',
    'read_at'
];

public $notificationTypes = [
    'dare' => [
        'App\Notifications\DareCreatedNotification',
        'App\Notifications\DareUpdatedNotification',
    ],
    'general' => [
        'App\Notifications\CommentCreatedNotification',
        'App\Notifications\LikeCreatedNotification',
        'App\Notifications\DareLikedNotification',
        'App\Notifications\SendEventInvitesNotification',
    ],
    'follower' => [
        'App\Notifications\FollowerUpdatedNotification'
    ]

];




}
