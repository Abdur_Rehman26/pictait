<?php

namespace App\Data\Models;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Event;

class Message extends Eloquent 
{
    use SoftDeletes;

	protected $connection = 'mongodb';
	protected $collection = 'messages';

	protected $fillable = [
		'user_one' , 'user_two' , 'type' , 'created_at' , 'updated_at'
	];



}