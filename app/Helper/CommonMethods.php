<?php
/**
 * Created by Sublime.
 * User: Kazmi
 * Date: 21/05/2018
 * Time: 07:24 AM
 */
use Carbon\Carbon;
use App\Data\Models\Notification;

function userImagePath($image, $folderId, $type = 'user'){

	if(substr($image, 0, 8) != "https://" && substr($image, 0, 8) != "http://"){

		if($type == 'user'){
			$path = config('uploads.images.user.public_relative').'/'.$folderId.'/';
		}elseif ($type == 'dare'){
			$path = config('uploads.images.dare.public_relative').'/'.$folderId.'/';

		}else{
			$path = config('uploads.images.event.public_relative').'/'.$folderId.'/';
		}

		$image = explode('.' ,$image);
		return $image[0] ? url($path.$image[0].'.'.$image[1]) : null;
	}
	return $image;
}

function notificationTypeAndText($data){
	$notificationType = [];
	$notificationData = $data->data;

	if($data->type == 'App\Notifications\CommentCreatedNotification'){
		$notificationType['type'] = 'comment';
		$notificationType['text'] = 'commented on your post';
	}

	if($data->type == 'App\Notifications\LikeCreatedNotification'){
		$notificationType['type'] = 'post';
		$notificationType['text'] = 'liked your post';
	}

	if($data->type == 'App\Notifications\DareCreatedNotification' || $data->type == 'App\Notifications\DareUpdatedNotification' || $data->type == 'App\Notifications\DareLikedNotification'){

		$notificationType['type'] = 'dare';
		$notificationType['text'] = 'challenged you for a dare';

		if(!empty($notificationData['action'])){

			if($notificationData['action'] == 'dare_accepted'){
				$notificationType['text'] = 'accepted your dare challenge';
			}elseif($notificationData['action'] == 'dare_created'){
				$notificationType['text'] = 'challege you for a dare';
			}elseif($notificationData['action'] == 'image_uploaded'){
				$notificationType['text'] = 'uploaded a photo';
			}elseif($notificationData['action'] == 'dare_liked'){
				$notificationType['text'] = 'liked your dare image';
			}else{
				$notificationType['text'] = 'dare your post';
			}
		}

	}

	if($data->type == 'App\Notifications\FollowerUpdatedNotification'){
		$notificationType['type'] = 'follower';
		$notificationType['text'] = 'requested to follow you';
	}



	if($data->type == 'App\Notifications\SendEventInvitesNotification'){
		$notificationType['type'] = 'event';
		$notificationType['text'] = 'invited you to an event';
	}

	return $notificationType;
}
