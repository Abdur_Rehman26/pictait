<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Data\Models\User;
use App\Notifications\DareLikedNotification;

class DareLikedJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $data;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $dareRepository = app('DareRepository');

        $data = $this->data;

        $event = new \StdClass();
        $dare = $dareRepository->findById($data['id']);
        $event->from = User::find($data['user_id']);
        $event->from->file_path = userImagePath($event->from->image, $event->from);
        $event->notifying_object = $dare;

        \Log::info($data['user_like_id'] != $data['user_id']);

        if($dare && $data['user_like_id'] != $data['user_id']){

            $event->to = User::find($data['user_like_id']);
            $event->text = 'liked your dare image';
            $event->to->file_path = userImagePath($event->to->image, $event->to);
            $event->to->notify(new DareLikedNotification($event));

        }

    }
}
