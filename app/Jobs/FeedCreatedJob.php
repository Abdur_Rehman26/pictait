<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Notifications\FeedCreatedNotification;
use App\Data\Models\Feed;
use App\Data\Models\Post;
use App\Data\Models\User;
use App\Data\Repositories\FeedRepository;


class FeedCreatedJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $data;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $feed = app('FeedRepository')->findById($this->data->id);
    
        $criteria = ['user_id' => $feed->posted_by_id , 'type' => 'user' , 'deleted_at' => null , 'requested' => false];
        $details = ['attributes' => ['follow_id']];

        $followingIds = app('FollowerRepository')->findAttributesOfCollectionByCriteria($criteria , false, $details);



        $event = new \StdClass();

        $post = Post::find($feed->post_id);
        $event->from = User::find((int)$feed->posted_by_id)->first();
        $event->from->file_path = userImagePath($event->from->image, $event->from);
        $event->notifying_object = $feed;

        if($post){
            
            foreach ($followingIds as $key => $followingId) {
                # code...
            
            $event->to = User::find($followingId)->first();
            $event->text = '';
            $event->to->file_path = userImagePath($event->to->image, $event->to);
            $event->to->notify(new FeedCreatedNotification($event));


            }


        }

    }
}
