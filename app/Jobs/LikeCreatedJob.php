<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Data\Models\User;
use App\Data\Models\Post;
use App\Notifications\LikeCreatedNotification;


class LikeCreatedJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $data;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $data = $this->data;

        $event = new \StdClass();
        $post = Post::find($data['post_id']);
        $event->from = User::find($data['user_id']);
        $event->from->file_path = userImagePath($event->from->image, $event->from);
        $event->notifying_object = $post;
        
        if($post){
            $event->to = User::find($post->user_id);
            $event->to->file_path = userImagePath($event->to->image, $event->to);
            $event->text = 'liked your post';
            $event->to->notify(new LikeCreatedNotification($event));
        }
        
    }
}
