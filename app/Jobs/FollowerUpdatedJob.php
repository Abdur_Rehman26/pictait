<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Notifications\FollowerUpdatedNotification;
use App\Data\Models\Follower;
use App\Data\Models\User;
use App\Data\Models\Notification;


class FollowerUpdatedJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $data;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $data = $this->data;

        $event = new \StdClass();

        if(!$data['deleted_at']){

            $criteria = [ 'user_id' => $data['user_id'], 'follow_id' => $data['follow_id']];

            $item = app('FollowerRepository')->findByCriteria($criteria);
            $event->from = User::find($data['user_id']);
            $event->from->file_path = userImagePath($event->from->image, $event->from);
            $event->notifying_object = $item;

            if($item){
                $event->to = User::find($data['follow_id']);
                $event->to->file_path = userImagePath($event->to->image, $event->to);
                $event->text = 'liked your post';
                $event->to->notify(new FollowerUpdatedNotification($event));
            }
            
        }

        if($data['deleted_at']){
            $criteria = ['notifiable_id' => $data['follow_id'] , 'type' => 'App\Notifications\FollowerUpdatedNotification', 'notifiable_type' => 'App\Data\Models\User'];
            if(Notification::where($criteria)->orderBy('created_at', 'desc')->first()){

                Notification::where($criteria)->orderBy('created_at', 'desc')->first()->delete();
            }

        }


        
    }
}
