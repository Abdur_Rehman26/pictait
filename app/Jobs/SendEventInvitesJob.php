<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Data\Models\Event;
use App\Data\Models\User;
use App\Notifications\SendEventInvitesNotification;

class SendEventInvitesJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $data;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {   
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $data = $this->data;

        $notification = new \StdClass();
        $eventData = Event::find($data['event_id']);
        $notification->notifying_object = $eventData;
        $notification->from = User::find($data['user_id']);
        
        if($eventData){

            foreach ($data['users'] as $key => $user) {
                # code...
            }

            $notification->to = User::find($user['id']);
            $notification->to->file_path = userImagePath($notification->to->image, $notification->to);
            $notification->text = 'invited you to an event';
            $notification->to->notify(new SendEventInvitesNotification($notification));
        }
        
    

    }
}
