<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use NotificationChannels\OneSignal\OneSignalChannel;
use NotificationChannels\OneSignal\OneSignalMessage;
use NotificationChannels\OneSignal\OneSignalWebButton;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Carbon\Carbon;

class MessageCreatedNotification  extends Notification implements ShouldBroadcast
{
    use Queueable, Dispatchable, InteractsWithSockets, SerializesModels;

    public $data;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($event)
    {
        $this->data = $event;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
    // 
        return [OneSignalChannel::class, 'broadcast'];
    }

    /**
    * Get the mail representation of the notification.
    *
    * @param  mixed  $notifiable
    * @return \Illuminate\Notifications\Messages\MailMessage
    */
    public function toOneSignal($notifiable)
    {
        \Log::info('toOneSignal');
        return OneSignalMessage::create()
        ->subject("Job bid updated")
        ->body('This is a test message')
        ->setData('test' , $this->data->body);

    }

    public function toBroadcast($notifiable)
    {   
        return new BroadcastMessage([
            'notifier_image' => $this->data->from->file_path,
            'from' => $this->data->from,
            'to' => $this->data->to,
            'notifier_name' => $this->data->from->first_name,
            'notifying_object' => $this->data->notifying_object,
            'notifier_id' => $this->data->from->id,
            'formatted_created_at' => Carbon::now()->diffForHumans()

        ]);
    }

    public function broadcastOn()
    {
        $channels = [
            'private-App.Data.Models.Message.'.$this->data->to->id,
        ];

        if(!$this->data->last_message || !empty($this->data->last_message->read_at)){
            $channels[] = 'private-App.Data.Models.Message.Count.'.$this->data->to->id;
        }

        return $channels;
    }

}
