<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="{{ asset('front/app.js') }}" defer></script>
    @include('initializers.facebook-initializer')
    

     <link rel="shortcut icon" href="{{ asset('/images/favicon.ico') }}">

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">
    <!-- Styles -->
    <link href="{{asset(mix('css/styles.css'))}}" rel="stylesheet">
    <link href="{{asset(mix('css/app.css'))}}" rel="stylesheet">
    <link href="{{asset(mix('css/custom.css'))}}" rel="stylesheet">
</head>
<body class="landing-page login-page">
    <div id="app">
        <canvas id="first" class="molecules__wrapper"></canvas>
        <div class="Aligner">
         <div class="card auth-wrapper">
             <span class="login-graphics"></span>
             <span class="login-corner-design"></span>
             <div class="login-content">
                <div class="">
                    <h1>Welcome to the Biggest Social Network in the World</h1>
                    <p>We are the best and biggest social network with 5 billion active users all around the world. Share you
                        thoughts, write blog posts, show your favourite music via Stopify, earn badges and much more!
                    </p>
                    <div class="registration-login-form">

                        <div class="tab-content">

                            <div class="tab-pane active" id="profile" role="tabpanel" data-mh="log-tab">
                                <!-- <h4 class="title "><center>Login to your Account</center></h4> -->
                                <div class="row">
                                    <div class="col-xl-12 col-lg-12 col-md-12">
                                        <a href="#" class="fb-connect-btn btn bg-facebook full-width btn-icon-left">

                                            <div class="loader-sm">
                                                <div class="button-loader">
                                                    <div class="loader">
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <i class="fa fa-facebook" aria-hidden="true"></i>Login with Facebook</a>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>                              
                    </div>                                             
                </div>
            </div>
        </div>
        <main class="py-4" style="display: none;">


            <div class="content-bg-wrap login-content-bg">
                <div class="content-bg"></div>
            </div>


            <!-- Login-Registration Form  -->

            <div class="container">
                <div class="row display-flex">
                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12">
                        <div class="landing-content">

                        </div>
                    </div>

                    <div class="col-xl-5 col-lg-6 col-md-12 col-sm-12 col-xs-12">

                    </div>
                </div>
            </div>


        </main>
    </div>
    <script type="text/javascript">
        var ParticlerDefaultConfig = (function () {
            function ParticlerDefaultConfig() {
                this.quantity = 40;
                this.lineWidth = 0.05;
                this.fillColor = "#fff";
                this.minSize = 1;
                this.maxSize = 3;
                this.minimalLineLength = 250;
                this.speed = 25;
                this.frameDuration = 20;
                this.backgroundColor = "transparent";
            }
            return ParticlerDefaultConfig;
        })();
        var Particler = (function () {
            function Particler(wrapperId, customConfig) {
                this.wrapperId = wrapperId;
                this.config = new ParticlerDefaultConfig();
                this.dotsArray = [];
                this.wrapper = document.getElementById(wrapperId);
                this.canvas = this.wrapper.getContext("2d");
                this.setConfig(customConfig);
                this.wrapper.style.backgroundColor = this.config.backgroundColor;
                this.setWrapperSize();
                this.generateDotsArray();
                this.resizingHandler();
            }
            Particler.prototype.setConfig = function (customConfig) {
                var _this = this;
                if (customConfig !== undefined) {
                    (function () {
                        var item;
                        for (item in _this.config) {
                            if (customConfig.hasOwnProperty(item) && _this.config[item] !== undefined) {
                                _this.config[item] = customConfig[item];
                            }
                        }
                    })();
                }
            };
            ;
            Particler.prototype.createDot = function (i, arr) {
                var size;
                var vx;
                var vy;
                var posX;
                var posY;
                var angle = Math.random() * 360;
                var rads = angle * Math.PI / 180;
        // set radom size and position
        size = Math.floor(Math.random() * (this.config.maxSize - this.config.minSize + 1) + this.config.minSize);
        posX = Math.random() * this.wrapper.offsetWidth;
        posY = Math.random() * this.wrapper.offsetHeight;
        vx = Math.cos(rads) * (this.config.speed / this.config.frameDuration);
        vy = Math.sin(rads) * (this.config.speed / this.config.frameDuration);
        arr[i] = {
            size: size,
            posX: posX,
            posY: posY,
            vx: vx,
            vy: vy
        };
    };
    Particler.prototype.drawDots = function () {
        var _this = this;
        var i;
        var j = this.config.quantity;
        var k;
        var el;
        var getDistance = function (x1, y1, x2, y2) {
            return Math.sqrt(Math.pow((x1 - x2), 2) + Math.pow((y1 - y2), 2));
        };
        for (i = 0; i < j; i++) {
            // define dots positions
            el = this.dotsArray[i];
            el.posX += el.vx;
            if (el.posX < 0 || el.posX > this.wrapper.offsetWidth) {
                if (el.posX < 0) {
                    el.posX = 0;
                }
                else {
                    el.posX = this.wrapper.offsetWidth;
                }
                el.vx = -el.vx;
            }
            el.posY += el.vy;
            if (el.posY < 0 || el.posY > this.wrapper.offsetHeight) {
                el.vy = -el.vy;
            }
            // draw dots
            this.canvas.beginPath();
            this.canvas.fillStyle = this.config.fillColor;
            this.canvas.arc(el.posX, el.posY, el.size, 0, 2 * Math.PI);
            // draw lines between dots
            for (k = 0; k < j; k++) {
                if (k !== i) {
                    this.canvas.lineWidth = this.config.lineWidth;
                    this.canvas.strokeStyle = this.config.fillColor;
                    this.canvas.moveTo(el.posX, el.posY);
                    if (getDistance(el.posX, el.posY, this.dotsArray[k].posX, this.dotsArray[k].posY) < this.config.minimalLineLength) {
                        this.canvas.lineTo(this.dotsArray[k].posX, this.dotsArray[k].posY);
                        this.canvas.stroke();
                    }
                }
            }
            this.canvas.fill();
        }
        this.canvas.fillStyle = this.config.fillColor;
        setTimeout(function () {
            _this.canvas.clearRect(0, 0, _this.wrapper.width, _this.wrapper.height);
            _this.drawDots();
        }, this.config.frameDuration);
    };
    Particler.prototype.generateDotsArray = function () {
        var i = 0;
        for (i; i < this.config.quantity; i++) {
            this.createDot(i, this.dotsArray);
        }
        this.drawDots();
    };
    Particler.prototype.setWrapperSize = function () {
        this.canvas.canvas.width = this.wrapper.offsetWidth;
        this.canvas.canvas.height = this.wrapper.offsetHeight;
    };
    Particler.prototype.resizingHandler = function () {
        var _this = this;
        window.addEventListener('resize', function () {
            _this.setWrapperSize();
        });
    };
    return Particler;
})();

var firstParticlerExample = new Particler("first");

var secondParticlerExample = new Particler("second", {
    quantity: 50,
    lineWidth: 2,
    fillColor: "black",
    minSize: 3,
    maxSize: 8,
    minimalLineLength: 50,
    speed: 40,
});

var thirdParticlerExample = new Particler("third", {
    fillColor: "#d3c1af",
    backgroundColor: "#2c3e50"
});
</script>
</body>
</html>
