import VueRouter from 'vue-router';

import NewsFeed from './components/NewsFeed.vue'
import Profile from './components/profile/Main.vue'
import ProfileMainHeader from './components/profile/MainHeader.vue'
import CreatePost from './components/posts/CreatePost.vue'
import Settings from './components/settings/Main.vue'
import Messages from './components/messages/Main.vue'

import EventMain from './components/events/Main.vue';
import EventHome from './components/events/Home.vue';
import EventUploadImages from './components/events/UploadImages.vue';
import EventAbout from './components/events/About.vue';
import EventFollowers from './components/events/Followers.vue';
import EventListingMain from './components/events/List.vue';
import EventSubmittedImages from './components/events/SubmittedImages.vue';
import EventCreate from './components/events/CreateEventForm.vue';
import EventCreateView from './components/events/EventCreateView.vue';





const router = new VueRouter({
    mode: 'history',
    routes: [
    {
        path: '/',
        component: NewsFeed,
        meta: {
            forAuth: true
        }
    },
    {
        path: '/post/:id',
        component: NewsFeed,
        name : 'post.show',
        meta: {
            forAuth: true
        }
    },
    
    {
        path : '/profile-user/:id',
        name : 'user.profile.main',
        component : Profile,
        meta: {
            forAuth: true
        },
        children: [
        {
            name : 'user.profile.timeline',
            path: '',
            component: require('./components/profile/Timeline.vue'),
            props : true
        },
        {
            name : 'user.profile.about',
            path: 'about',
            component: require('./components/profile/About.vue'),
            props : true
        },
        {
            name : 'user.profile.dare',
            path: 'dare',
            component: require('./components/profile/Dare.vue'),
            props : true
        },
        {
            name : 'user.profile.follower',
            path: 'follower',
            component: require('./components/profile/Friends.vue'),
            props : true
        },
        {
            name : 'user.profile.following',
            path: 'following',
            component: require('./components/profile/Followings.vue'),
            props : true
        },
        {
            name : 'user.profile.event',
            path: 'event',
            component: require('./components/profile/Events.vue'),
            props : true
        }  
        ]            
    },
    {
        path : '/settings',
        name : 'settings',
        component : Settings,
        meta: {
            forAuth: true
        }
    },

    {
        path : '/messages',
        name : 'messages',
        component : Messages,
        meta: {
            forAuth: true
        }
    },
    {
        path: '/events',
        name : 'events.main',
        component : EventListingMain,
        meta: {
            forAuth: true
        }
    },
    

    {
        path: '/event/create',
        component : EventCreateView,
        meta: {
            forAuth: true
        },

        children: [

        {
            path: '/',
            name : 'event.create',
            component : EventCreate,
            meta: {
                forAuth: true
            }
        },
        {
            path: ':id/upload-image',
            name : 'event.upload-image',
            component : EventCreate,
            meta: {
                forAuth: true
            }       
        },


        ]
    },

    {
        path: '/event/:id',
        component : EventMain,
        meta: {
            forAuth: true
        },

        children: [
        {
            name : 'event.followers',
            path: 'followers',
            component: EventFollowers,
            props : true
        },
        {
            name : 'event.about',
            path: 'about',
            component : EventAbout,
            meta: {
                forAuth: true
            }       
        },  
        {
            path: 'home',
            name : 'event.home',
            component : EventHome,
            meta: {
                forAuth: true
            }       
        },
        {
            path: 'submitted-images',
            name : 'event.submitted-images',
            component : EventSubmittedImages,
            meta: {
                forAuth: true
            }       
        },
        {
            path: 'upload-images',
            name : 'event.upload-images',
            component : EventUploadImages,
            meta: {
                forAuth: true
            }       
        },



        ]

    },


    ],
});

export default router
