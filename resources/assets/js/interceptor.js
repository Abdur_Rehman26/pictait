import Router from './routes';

Vue.http.interceptors.push((request, next) => {

if(!Vue.auth.getToken()){
	window.location.href = '/login';
}

request.headers.set('Authorization', 'Bearer '+Vue.auth.getToken())
request.headers.set('Accept', 'application/json')
next()

// return response callback
  return function(response) {

  	if(response.status == 401){
		window.location.href = '/login';
  	}

  };

});