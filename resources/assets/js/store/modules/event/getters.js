export default {
    session: (state) => state.session,
    sessionTeam: (state) => state.sessionTeam,
    sessionTeamMembers: (state) => state.sessionTeamMembers,
}
