export default {
    setSession(state, payload) {
        state.session = payload
    },
    setSessionTeam(state, payLoad) {
        state.sessionTeam = payLoad
    },
    setSessionTeamMembers(state, payLoad) {
        state.sessionTeamMembers = payLoad
    },
}
