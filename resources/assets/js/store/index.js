import Vue from "vue"
import Vuex from "vuex"
import event from "./modules/event/"
import auth from "./modules/auth/"

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== "production"

export default new Vuex.Store({
    modules: {
    	auth,
        event,
    },
    strict: debug
})
