import Router from './routes';
  
Router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.forVisitors)) {
  if (Vue.auth.getToken()) {
    next({
      path: '/'
    })
  }
  else next()
}
else if (to.matched.some(record => record.meta.forAuth)) {
  if (!Vue.auth.getToken()) {
    next({
      path: '/'
    })
  }
  else next()
}
else{
  next()
}
});


Router.afterEach((to, from) => {

});