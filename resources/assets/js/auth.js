export default function (Vue) {
    let authenticatedUser = {};
    Vue.auth = {
        setToken (token, expiration){
            localStorage.setItem('token', token);
            localStorage.setItem('expiration', expiration);
        },
        getToken (){
            var token = localStorage.getItem('token');
            var expiration = localStorage.getItem('expiration');
            if (!token || !expiration) {
                return null;
            }
            return token;
        },
        destroyToken(){
            authenticatedUser = null;
            localStorage.removeItem('token');
            localStorage.removeItem('expiration');
        },
        setAuthenticatedUser(data){
            this.$store.commit('authUser' , data)
        },
        getAuthenticatedUser(){
            this.$store.getters.authUser();
        }

    };

    Object.defineProperties(Vue.prototype, {
        $auth: {
            get: () => {
            return Vue.auth
        }
    }
});


}