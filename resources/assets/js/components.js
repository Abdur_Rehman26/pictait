import Vue from 'vue';


Vue.component('example', require('./components/ExampleComponent.vue'));


// Partials 
Vue.component('guidance-popup', require('./components/common/NotificationPopup.vue'));
Vue.component('navbar-component', require('./components/partials/NavBar.vue'));
Vue.component('navbar-main-component', require('./components/partials/Main.vue'));
Vue.component('sidebar-right-component', require('./components/partials/SideBarRight.vue'));
Vue.component('sidebar-left-component', require('./components/partials/SideBarLeft.vue'));
Vue.component('search-navbar', require('./components/partials/SearchNavBar.vue'));



// Popups

Vue.component('choose-from-photos', require('./components/popups/ChooseFromPhotos.vue'));
Vue.component('upload-dare-image', require('./components/popups/UploadDareImage.vue'));
Vue.component('upload-image', require('./components/popups/UploadImage.vue'));
Vue.component('dare-images', require('./components/popups/DareImages.vue'));



// Profile 

Vue.component('profile-main-header', require('./components/profile/MainHeader.vue'));
Vue.component('profile-timeline', require('./components/profile/Timeline.vue'));
Vue.component('profile-friend', require('./components/profile/Friends.vue'));
Vue.component('profile-followings', require('./components/profile/Followings.vue'));
Vue.component('profile-gallery', require('./components/profile/Gallery.vue'));
Vue.component('profile-about', require('./components/profile/About.vue'));
Vue.component('profile-dare', require('./components/profile/Dare.vue'));
Vue.component('profile-event', require('./components/profile/Events.vue'));
Vue.component('profile-personal-info', require('./components/profile/PersonalInfo.vue'));
Vue.component('profile-intro-block', require('./components/profile/ProfileIntroBlock.vue'));


// Settings

Vue.component('settings-account', require('./components/settings/Account.vue'));
Vue.component('settings-change-password', require('./components/settings/ChangePassword.vue'));
Vue.component('settings-main-header', require('./components/settings/MainHeader.vue'));
Vue.component('settings-account', require('./components/settings/Account.vue'));
Vue.component('settings-change-password', require('./components/settings/ChangePassword.vue'));
Vue.component('settings-right-sidebar', require('./components/settings/RightSidebar.vue'));
Vue.component('settings-personal', require('./components/settings/Personal.vue'));
Vue.component('settings-profile-image', require('./components/settings/ProfileImage.vue'));



// Posts 

Vue.component('create-post', require('./components/posts/CreatePost.vue'));
Vue.component('post', require('./components/posts/Post.vue'));
Vue.component('posts', require('./components/posts/Posts.vue'));
Vue.component('single-post', require('./components/posts/SinglePost.vue'));

Vue.component('feeds', require('./components/Feeds.vue'));


Vue.component('friend-card', require('./components/FriendCard.vue'));
Vue.component('photo-card', require('./components/PhotoCard.vue'));
Vue.component('photo-cards', require('./components/PhotoCards.vue'));
Vue.component('friends-thumb-block', require('./components/FriendsThumbBlock.vue'));
Vue.component('photos-thumb-block', require('./components/PhotosThumbBlock.vue'));
Vue.component('home-profile-block', require('./components/HomeProfileBlock.vue'));

// Navbar

Vue.component('notifications-block', require('./components/navbar/NotificationsBlock.vue'));
Vue.component('messages-block', require('./components/navbar/MessagesBlock.vue'));
Vue.component('friends-request-block', require('./components/navbar/FriendsRequestBlock.vue'));
Vue.component('friend-request-li', require('./components/navbar/FriendRequest.vue'));
Vue.component('profile-dropdown-block', require('./components/navbar/ProfileDropdownBlock.vue'));
Vue.component('dare-request-block', require('./components/navbar/DaresRequestBlock.vue'));
Vue.component('dare-request-li', require('./components/navbar/DareRequest.vue'));


// Comments

Vue.component('comments', require('./components/comments/Comments.vue'));
Vue.component('comment', require('./components/comments/Comment.vue'));
Vue.component('create-comment', require('./components/comments/CreateComment.vue'));



// Messges 

Vue.component('messages-sidebar', require('./components/messages/Sidebar.vue'));
Vue.component('messages-main-panel', require('./components/messages/MainPanel.vue'));
Vue.component('messages-compose', require('./components/messages/ComposeMessage.vue'));
Vue.component('message-li', require('./components/messages/SingleMessage.vue'));



// common Components 
// 
Vue.component('no-messages-found', require('./components/common/NoMessagesFound.vue'));
Vue.component('no-records-found', require('./components/common/NoRecordFound.vue'));
Vue.component('upload-image-component', require('./components/common/UploadImage.vue'));
Vue.component('search-multi-select', require('./components/common/SearchMultiSelect.vue'));
Vue.component('common-methods', require('./components/common/CommonMethods.vue'));
Vue.component('vue-pagination', require('./components/common/Pagination.vue'));
Vue.component('block-spinner', require('./components/common/BlockSpinner.vue'));
Vue.component('spinner-loader', require('./components/common/SpinnerLoader.vue'));
Vue.component('confirm-popup', require('./components/common/ConfirmationPopUp.vue'));
Vue.component('account-protected', require('./components/common/AccountProtected.vue'));
Vue.component('loader', require('./components/common/Loader.vue'));
Vue.component('alert-messages', require('./components/common/Alert.vue'));
Vue.component('image-upload', require('./components/common/ImageUpload.vue'));
Vue.component('dropzone-component', require('./components/common/DropZoneComponent.vue'));



/* Event */

Vue.component('event-top-header', require('./components/events/TopHeader.vue'));
Vue.component('create-event-form', require('./components/events/CreateEventForm.vue'));
Vue.component('event-main', require('./components/events/Main.vue'));
Vue.component('event-home', require('./components/events/Home.vue'));
Vue.component('event-menu-bar', require('./components/events/MainMenuBar.vue'));
Vue.component('event-main-title', require('./components/events/MainTitle.vue'));
Vue.component('event-banner-area', require('./components/events/BannerArea.vue'));
Vue.component('event-entry', require('./components/events/EventEntry.vue'));
Vue.component('event-upload-images', require('./components/events/UploadImages.vue'));
Vue.component('event-about', require('./components/events/About.vue'));
Vue.component('event-followers', require('./components/events/Followers.vue'));
Vue.component('event-submit-image', require('./components/events/EventSubmitImage.vue'));
Vue.component('event-submitted-images', require('./components/events/SubmittedImages.vue'));
Vue.component('event-images-limit', require('./components/events/EventImagesLimit.vue'));
Vue.component('event-thumbnail', require('./components/events/EventThumbnail.vue'));
Vue.component('event-list-thumbnail', require('./components/events/EventListThumbnail.vue'));
Vue.component('event-search-sidebar', require('./components/events/SearchSidebar.vue'));




Vue.component('dynamic-link', {
  template: '<component v-bind:is="transformed"></component>',
  props: ['text'],
  methods: {
    convertHashTags: function(str) {
      const spanned = `<span>${str}</span>`
      return spanned.replace(/#([\w]+)/g,'<router-link to="/tag/$1">#$1</router-link>')
    }
  },
  computed: {
    transformed () {
      const template = this.convertHashTags(this.text);
      return {
        template: template,
        props: this.$options.props
      }
    }
  }
})

