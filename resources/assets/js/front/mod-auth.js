//Require Module
app = require('./config');
//Define Module

app.SocialAccount = (function () {
    var config = app.Config;
    var facebookData = Array();
    var dataApiUrl = 'api/user/';
    var dataAppUrl = config.getAppUrl();
    var deleteId = '';
    //Limit for listing
    var dataListLimit = 20;
    var dataListStart = 0;
    var ulSortBy = 'most_followers';


    var getLongLivedToken = function (data , successCallback) {

        var request = $.ajax({
            url: dataApiUrl + 'external-refresh-token',
            data: data,
            type: 'POST'
        });

        request.done(function (data) {
            if (data.response.code == 200) {
                successCallback();
            }
        });

        request.fail(function (jqXHR, textStatus) {

        });

    };

    //Connect with Facebook
    var ConnectWithFacebook = function (successCallback, errorCallback) {
        FB.login(function (response) {
    
            if (response.status === 'connected') {
                facebookData['facebook_token'] = FB.getAuthResponse()['accessToken'];
                FB.api('/me?fields=name,email,gender', function (resp) {
                    facebookData['facebook_id'] = response.authResponse.userID;
                    facebookData['name'] = resp.name;
                    facebookData['gender'] = resp.gender;
                    facebookData['email'] = resp.email;
                    facebookData['image'] = "";
                    FB.api('/me/picture?width=480&height=480', function (resp2) {
                        facebookData['image'] = resp2.data ? resp2.data.url : '';
                        successCallback(facebookData);                        
                   });
                });

            }
            else {
            }
        }, {scope: ['email'], return_scopes: true});
    };

    var logout = function (data , successCallback) {
    
        var request = $.ajax({
            url: '/api/user/logout',
            data: data,
            type : 'POST'
        });


        request.done(function (data) {
            if (data.response.code == 200) {
                successCallback(data);
            }
        });

        request.fail(function (jqXHR, textStatus) {

        });

    };

    //Create Account
    var create = function (data , successCallback) {
        var request = $.ajax({
            url: 'api/user',
            data: data,
            type : 'POST'
        });


        request.done(function (data) {
            if (data.data) {
                successCallback(data);
            }
        });

        request.fail(function (jqXHR, textStatus) {

        });
    };


    //Create Account
    var list = function (successCallback) {
        var request = $.ajax({
            url: dataApiUrl + 'list',
            type: 'GET',
            headers: {"Authorization": "Bearer " + dataTokenGet}
        });

        request.done(function (response) {
                var data = response.data.data;
                successCallback(data);
        });

        request.fail(function (jqXHR, textStatus) {


        });
    };


    //Create Account
    var destroy = function (successCallback) {
        var request = $.ajax({
            url: dataApiUrl + 'delete/'+app.SocialAccount.deleteId,
            type: 'GET',
            headers: {"Authorization": "Bearer " + dataTokenGet}
        });

        request.done(function (response) {
            successCallback(response);
        });

        request.fail(function (jqXHR, textStatus) {
            $('#delete-popup-id .btn-transparent').removeClass('pointer-events-none');

        });
    };

    return {
        getLongLivedToken : getLongLivedToken,
        ConnectWithFacebook: ConnectWithFacebook,
        create: create,
        list: list,
        destroy : destroy,
        logout : logout
    }
})();
//Export Module
module.exports = app.SocialAccount;