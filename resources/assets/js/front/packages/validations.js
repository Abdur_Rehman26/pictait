app = require('../config');

var errors;

$(document).on('submit', '.cg-submit-form', function (e) {

    errors = [];

    e.preventDefault();

    //  Form object
    var submissionForm = $(this);


    // form submit button must have this class cg-submit
    var formSubmit = submissionForm.attr('cg-submit');

    // form main class
    var formClass = 'cg-submit-form';

    // front end error messages time out which needs to be set in html
    var jsMessageTimeout = submissionForm.attr('cg-message-timeout');

    // server end error messages time out which needs to be set in html
    var jsServerMessageTimeout = submissionForm.attr('cg-message-response-timeout');

    // removing all the alerts and messages before validating the form
    submissionForm.find('.alert').removeClass('alert-danger').removeClass('alert-success').html('').hide();

    // getting fields to validate. You can add or remove classes in order to get more fields
    var inputFields = submissionForm.find('*').filter('input, select').filter(':not(".cg-no-validate")').filter(':not(".cg-no-validate")');

    // validating form here
    var form_check = validateFormFields(submissionForm, inputFields);


    // "errors" is a global variable which is populated in the previous "validateForms" method
    if (!form_check) {
        submissionForm.find('.alert').addClass('alert-danger').removeClass('alert-success').html(errors[0]).show();
    }

    // hide message after timeout set which needs to be set in html form as a cg-message-timeout field
    if (typeof(jsMessageTimeout) !== 'undefined' && !form_check) {
        setTimeout(function () {
            clearMessageField(formClass);

        }, +jsMessageTimeout)
    }


    if (formSubmit && form_check) {

        submitForm(submissionForm, function (success) {

        }, function (error) {

        }, function (done) {

            // hide server message after timeout set which needs to be set in html form as a cg-response-message-timeout field
            if (typeof(jsServerMessageTimeout) !== 'undefined') {
                setTimeout(function () {
                    clearMessageField(formClass);

                }, +jsServerMessageTimeout)
            }
        })

    }


});


var submitForm = function (submissionForm, errorCallBack, successCallBack, doneCallBack) {

    var message = submissionForm.attr('success-message');
    var submissionMethod = submissionForm.attr('method');
    var submissionButton = submissionForm.find('.cg-submit-btn');
    var submissionActionUrl = submissionForm.attr('action');
    var data = submissionForm.serialize();
    var submissionData = {data: data, url: submissionActionUrl, method: submissionMethod};

    submissionButton.addClass('show-spinner');

    submitCall(submissionData, function (successResponse) {

        var successMessage = [];
        if (typeof(message) === 'undefined' || message === '') {
            message = successResponse.response.message;
        }

        successMessage.push(message);

        displayAlert('cg-submit-form', 'success', successMessage);
        successCallBack(successResponse);

    }, function (errorResponse) {

        $.each(errorResponse.errors, function (errorIndex, errorValue) {
            errors.push(errorValue);
        });
        displayAlert('cg-submit-form', 'error', errors);

        errorCallBack(errorResponse);

    }, function (done) {

        submissionButton.removeClass('show-spinner');
        doneCallBack(done);

    });

    return false;
};


var submitCall = function (submissionData, successCallback, errorCallback, doneCallback) {
    var data = submissionData.data;
    var action_url = submissionData.url;
    var method = submissionData.method;

    var ajax_request = $.ajax({
        url: app.Config.getApiUrl() + action_url,
        data: data,
        type: method,
        dataType: 'json',
    });

    ajax_request.done(function (data) {
        successCallback(data);
        doneCallback();
    });

    ajax_request.fail(function (jqXHR, textStatus) {
        errorCallback(jqXHR.responseJSON);
        doneCallback();
    });

};

function clearValidationFields(className) {
    var inputFields = $('.' + className).find('*').filter('input, select').filter(':not(".cg-no-validate")').filter(':not(".cg-no-validate")');
    $.each(inputFields, function (index, value) {
        var field = $(value);
        field.val('');
        field.closest('.form-group').removeClass('has-error');
    });
}

function clearMessageField(className) {
    $('.' + className).find('.alert').removeClass('alert-success').removeClass('alert-danger').html('');
}


function validateFormFields(main_form_div, fieldsIncludeList, fieldsExcludeList, additionalConditions) {
    var form_check = true;
    var fields_to_check = 'select , input';
    fields_to_check = fieldsIncludeList ? fieldsIncludeList : fields_to_check;

    $.each(main_form_div.find(fields_to_check), function (index, value) {
        var field = $(value);
        var fieldValue = field.val();
        var fieldName = field.attr('name');
        var fieldTitle = field.attr('title');
        var fieldType = field.attr('type');
        var fieldTag = value.tagName.toLowerCase();
        var fieldErrorMessage = field.attr('message');

        var errorMessage = '';

        field.closest('.form-group').removeClass('has-error');


        if (fieldValue === '' || fieldValue === -1 || fieldValue === null) {
            form_check = false;

            if (typeof(fieldTitle) === 'undefined' || !fieldTitle) {
                fieldTitle = fieldName;
            }

            errorMessage = toTitleCase(fieldTitle) + ' field is required';
        }

        if (fieldName === 'email' && fieldValue !== '') {
            if (!isValidEmailAddress(fieldValue)) {
                errorMessage = 'The email address is invalid';
                form_check = false;
            }
        }

        if (fieldValue !== '') {

            // These validations are for field if value is available 


            if (field.attr('cg-validate-length') && fieldValue.length < parseInt(field.attr('cg-validate-length'))) {

                if (typeof(fieldTitle) === 'undefined' || !fieldTitle) {
                    fieldTitle = fieldName;
                }

                errorMessage = 'The ' + fieldTitle + ' length must be greater than ' + field.attr('cg-validate-length') + ' characters';
                form_check = false;
            }

            if (field.attr('cg-validate-same') && typeof($('input[name="' + field.attr('cg-validate-same') + '"]')) !== 'undefined' && $('input[name="' + field.attr('cg-validate-same') + '"]').val() !== fieldValue) {

                if (typeof(fieldTitle) === 'undefined' || !fieldTitle) {
                    fieldTitle = fieldName;
                }
                errorMessage = 'The ' + fieldTitle + ' does not match ' + field.attr('cg-validate-same');
                form_check = false;
            }

            if (fieldType === 'checkbox') {
                if (!field.is(":checked")) {
                    errorMessage = 'Please agree to terms and conditions.';
                    form_check = false;
                }
            }

            // Add conditions here for other validations

        }

        if (fieldTag === 'select') {
            if (typeof(additionalConditions) !== 'undefined') {
                $.each(additionalConditions['select'], function (additionalIndex, additionalValue) {
                    var match_expression = '';
                    // Removing operator for now on
                    if (field.hasClass(additionalValue.fieldClass)) {
                        match_expression = fieldValue + ' ' + additionalValue.operator + ' ' + additionalValue.value;
                        if (match_expression) {
                            form_check = false;
                        }
                    } else {
                        match_expression = '"' + fieldValue + '" ' + additionalValue.operator + ' "' + additionalValue.value + '"';
                        if (eval(match_expression)) {
                            form_check = false;
                        }
                    }
                });
            }
        }
        // extra conditions  here


        if (typeof(fieldErrorMessage) !== 'undefined' && fieldErrorMessage) {
            errorMessage = fieldErrorMessage;
        }

        if (!form_check) {
            field.closest('.form-group').addClass('has-error');
            errors.push(errorMessage);
        }


    });
    return form_check;
}

function toTitleCase(str) {
    return str.replace(/\w\S*/g, function (txt) {
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    });
}

//Validate Email Address Pattern
function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
    return pattern.test(emailAddress);
};


function displayAlert(formClassName, type, messages) {
    if (type === 'error') {
        $('.' + formClassName).find('.alert').addClass('alert-danger').removeClass('alert-success').html(messages[0]).show();
    } else {
        $('.' + formClassName).find('.alert').addClass('alert-success').removeClass('alert-danger').html(messages[0]).show();
    }
}