//Base Framework
require('../bootstrap');

//Core Libraries

$(document).ready(function () {


    require('./event-auth');
    require('./event-profile');
    require('./event-profile-settings');




    //Front End Scripts
    require('./packages/validations');

	require('./mod-auth');
});
