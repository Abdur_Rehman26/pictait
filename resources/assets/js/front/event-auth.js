//Require Module
app = require('./config');
var config = app.Config;
var dataAppUrl = config.getAppUrl();


$('.fb-connect-btn').click(function (e) {

    $('.fb-connect-btn').addClass('show-spinner');

    e.preventDefault();
    /* Act on the event */
    app.SocialAccount.ConnectWithFacebook(function (data) {
        var newData = {
            provider_access_token : data['facebook_token'],
            email : data['email'],
            first_name: data['name'],
            gender : data['gender'],
            provider_id: data['facebook_id'],
            image: data['image']
        };

        app.SocialAccount.create(newData, function (response) {


            localStorage.setItem('token', response.data.token);
            localStorage.setItem('expiration', response.data.expiration);

            let url = '/';

            if(response.data.user.first_time_user){
                url += '?first_time_user=true';
            }


            window.location.href = url;

        });
    });

});

$('.log-out').click(function () {
    app.SocialAccount.logout();
});
