//Global
var app = app || {};
window.app = app;

//Define Module
app.Config = (function(){

	var getApiUrl = function(){
        return  '/api';
    };
	var getAppUrl = function(){
		return $('base').attr('href');
	};
	


    return {
		getApiUrl:getApiUrl,
		getAppUrl:getAppUrl
	}

}());
//Export Module
module.exports = app;