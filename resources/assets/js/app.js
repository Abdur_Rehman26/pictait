/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

 require('./bootstrap');

 window.Vue = require('vue');

 import Vue from 'vue';
 import Vuex from 'vuex';
 import VueRouter from 'vue-router';
 import VueResource from 'vue-resource';
 import VeeValidate from 'vee-validate';
 import store from './store/index';

 import router from './routes';
 import Auth from './auth';
 import BootstrapVue from 'bootstrap-vue';
 import { directive as onClickaway } from 'vue-clickaway';

 import { Datetime } from 'vue-datetime';
// You need a specific loader for CSS files
import InfiniteLoading from 'vue-infinite-loading';
import Multiselect from 'vue-multiselect';
import { library } from '@fortawesome/fontawesome-svg-core'
import { faUserFriends } from '@fortawesome/free-solid-svg-icons'
import { faEnvelopeOpen } from '@fortawesome/free-solid-svg-icons'
import { faBell } from '@fortawesome/free-solid-svg-icons'
import { faImages } from '@fortawesome/free-solid-svg-icons'
import { faCog } from '@fortawesome/free-solid-svg-icons'
import "intro.js/minified/introjs.min.css";

import VueLazyload from 'vue-lazyload'
import vue2Dropzone from 'vue2-dropzone'
import 'vue2-dropzone/dist/vue2Dropzone.min.css'

var VueScrollTo = require('vue-scrollto');


// You can also pass in the default options
Vue.use(VueScrollTo, {
	container: "body",
	duration: 500,
	easing: "ease",
	offset: 0, 
	force: true,
	cancelable: true,
	onStart: false,
	onDone: false,
	onCancel: false,
	x: false,
	y: true
})

Vue.use(vue2Dropzone);

// or with options
Vue.use(VueLazyload, {
	preLoad: 6.3,
	loading: '/images/Spinner-1s-200px.svg',
	attempt: 1
})

import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

import 'vue-datetime/dist/vue-datetime.css'

Vue.use(Datetime)


Vue.use(VueRouter);
Vue.use(VueResource);
Vue.use(Vuex);
Vue.use(Auth);

// Font Awesome Icons 
library.add(faUserFriends)
library.add(faEnvelopeOpen)
library.add(faBell)
library.add(faImages)

Vue.component('font-awesome-icon', FontAwesomeIcon)


// custom packages
Vue.use(InfiniteLoading);
Vue.use(Multiselect);
Vue.use(BootstrapVue);


// compnents 
// 
Vue.component('datetime', Datetime);
Vue.component('multiselect', Multiselect);


require('./route-middleware');
require('./interceptor');
import './components';
require('./filter');
require('./directive');

const config = {
    errorBagName: 'errorBag', // change if property conflicts.
};

Vue.use(VeeValidate, config);

// Resource Configurations 
Vue.http.options.root = '/api';



// Laravel Echo 

import Echo from 'laravel-echo'
window.io = require('socket.io-client');

let token = document.head.querySelector('meta[name="csrf-token"]');

window.Echo = new Echo({
	broadcaster: 'socket.io',
	host: window.location.hostname + ':6001',
	auth: {
		headers: {
            Authorization: 'Bearer ' + Vue.auth.getToken(),//token.content,
        },
    },
});



const app = new Vue({
	el: '#app',
	router: router,
	store
});
