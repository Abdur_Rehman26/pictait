<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['auth:api']], function () {

    Route::resource('country', 'Api\V1\CountryController')->except([
        'edit'
    ]);

    Route::resource('comment', 'Api\V1\CommentController')->except([
        'edit'
    ]);

    Route::get('dare/count' , 'Api\V1\DareController@getUnreadCount')->name('dare.count');        
    Route::put('dare/like/{id}', 'Api\V1\DareController@dareLike')->name("dare.like");
    Route::put('dare/pin-to-profile/{id}', 'Api\V1\DareController@pinToProfile')->name("dare.like");
    Route::resource('dare', 'Api\V1\DareController')->except([
        'edit'
    ]);


    Route::post('event/send-invite', 'Api\V1\EventController@sendInvite')->name("event.send-invite");
    Route::resource('event', 'Api\V1\EventController')->except([
       'edit'
   ]);

    Route::post('event-entry/individual', 'Api\V1\EventEntryController@uploadUserImage')->name("event-entry.individual");
    Route::put('event-entry/individual/approve/{id}', 'Api\V1\EventEntryController@approve')->name("event-entry.individual.approve");
    Route::put('event-entry/update-like/{id}', 'Api\V1\EventEntryController@updateLike')->name("event-entry.update.likes");
    Route::resource('event-entry', 'Api\V1\EventEntryController')->except([
       'edit'
   ]);

    Route::resource('feed', 'Api\V1\FeedController')->except([
        'edit'
    ]);
//Uploading File
    Route::post('file/upload', 'Api\V1\FileUploadController@upload')->name("file.upload");
    Route::post('file/remove', 'Api\V1\FileUploadController@remove')->name("file.remove");


    Route::get('follower/count' , 'Api\V1\FollowerController@getUnreadCount')->name('follower.count');        
    Route::resource('follower', 'Api\V1\FollowerController')->except([
        'edit'
    ]);


    Route::resource('like', 'Api\V1\LikeController')->except([
        'edit'
    ]);

    Route::resource('message', 'Api\V1\MessageController')->except([
        'edit'
    ]);

    Route::resource('message-response', 'Api\V1\MessageResponseController')->except([
        'edit'
    ]);

    Route::post('notification/read' , 'Api\V1\NotificationController@markAsRead')->name('notification.mark.read');
    Route::get('notification/count' , 'Api\V1\NotificationController@getUnreadCount')->name('notification.count');
    Route::resource('notification', 'Api\V1\NotificationController')->except([
        'edit'
    ]);


    Route::resource('post', 'Api\V1\PostController')->except([
        'edit'
    ]);


    Route::resource('setting', 'Api\V1\SettingController')->except([
        'edit'
    ]);

    Route::get('user/me' , 'Api\V1\UserController@getAuthUser');
    Route::get('user/search/{id}' , 'Api\V1\UserController@search');
    Route::post('user/profile/image' , 'Api\V1\UserController@updateImage');
    Route::post('user/change-password' , 'Api\V1\UserController@changePassword');
    Route::get('user/logout', 'Api\V1\UserController@logout');
    Route::resource('user', 'Api\V1\UserController')->except([
        'edit','store'
    ]);

    Route::resource('user-detail', 'Api\V1\UserDetailController')->except([
        'edit'
    ]);

    Route::resource('follower', 'Api\V1\FollowerController')->except([
        'edit'
    ]);

});Route::resource('role', 'Api\V1\RoleController')->except([
    'edit'
]);Route::resource('roleuser', 'Api\V1\RoleUserController')->except([
    'edit'
]);Route::resource('hall', 'Api\V1\HallController')->except([
             'edit'
        ]);Route::resource('page', 'Api\V1\PageController')->except([
             'edit'
        ]);