<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/login', function () {
    return view('login');
});

Route::group([
    'middleware' => [
        'user.auth',
    ],
], function(){


Route::get('/', 'HomeController@authLogin');



});


Route::middleware(['api' , 'user.session'])->post('api/user', 'Api\V1\UserController@store');
Route::middleware(['api' , 'user.session'])->post('api/user/logout', 'Api\V1\UserController@logout');



Route::group([
    'middleware' => [
        'user.guest',
    ],
], function(){


Route::get('/', function () {
    return view('layouts.app');
});

});

Route::get('/{any}', function (){
    return view('layouts.app');
})->where('any', '.*');

