let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.styles([
    'resources/assets/css/custom.css'
], 'public/css/custom.css');

mix.js('resources/assets/js/front/app.js', 'public/js')
	.js('resources/assets/js/app.js', 'public/vue.js')
	.js('resources/assets/js/front/app.js', 'public/front/app.js')
   	.sass('resources/assets/sass/styles.scss', 'public/css')
   	.sass('resources/assets/sass/app.scss', 'public/css');

mix.copyDirectory('resources/assets/img/', 'public/img/');
mix.copyDirectory('resources/assets/fonts', 'public/fonts');
mix.copyDirectory('resources/assets/icons', 'public/icons');
mix.copyDirectory('resources/assets/images', 'public/images');
// Front
mix.version();
