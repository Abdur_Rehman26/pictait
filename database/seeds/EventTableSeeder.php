<?php

use Illuminate\Database\Seeder;
use App\Data\Models\User;
use Carbon\Carbon;
use Faker\Factory as Faker;

class EventTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$faker = Faker::create();
    	$date = Carbon::now()->toDateTimeString();

    	$imagesArray = [
    		'Top Actors' => 'event-hollywood-actors.jpg',
            'Top Movies' => 'event-hollywood-movies.jpeg',
            'Top Footballers' => 'event-top-footballers.png'
        ];

        $data = [];

        $user = User::where('email' , 'like' , '%sark_92@hot%')->first();

        if(!empty($user)){
            foreach ($imagesArray as $key => $imageArray) {
                $data [] = [
                    'image' => config('app.url') . '/images/' . $imageArray,
                    'description' => $faker->text,
                    'user_id' => $user['id'],
                    'name' => $key,
                    'event_type' => 'competition',
                    'number_of_images' => 10,
                    'privacy_type' => 'private',
                    'created_at' => $date,
                    'updated_at' => $date,

                ];

            }

            \App\Data\Models\Event::insert($data);
        }


    }
}
