<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Data\Models\User;
use Carbon\Carbon;

class MessageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$faker = Faker::create();

    	$userIds = User::limit(150)->pluck('id')->toArray();

    	foreach ($userIds as $key => $userId) {


    		foreach (range(1, 500) as $index) {

    			$date = Carbon::create(2018, 5, 28, 0, 0, 0);

    			$recipientId = User::inRandomOrder()->where('id', '!=' , $userId)->limit(1)->pluck('id')->toArray();


    			$data['user_id'] = $userId;
    			$data['recipient_id'] = $recipientId[0];
    			$data['reply'] = $faker->realText;

    			app('MessageResponseRepository')->store($data);


    		}
    	}
    }
}
