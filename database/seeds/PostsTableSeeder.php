<?php

use Illuminate\Database\Seeder;
use App\Data\Models\User;
use App\Data\Models\Post;
use Carbon\Carbon;
use Faker\Factory as Faker;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$faker = Faker::create();

        $imagesArray = config('dummyImages');

        foreach (range(1, 500) as $index) {


            $date = Carbon::create(2018, 5, 28, 0, 0, 0);


            $userIds = User::inRandomOrder()->limit(1)->pluck('id')->toArray();

            $data = [

             'caption' => $faker->text,
             'user_id' => $userIds[0],
             'posted_by' => 'user',
             'posted_by_id' => $userIds[0],
             'likeCount' => 0,
             'likes' => [],
             'file_name' => $imagesArray[array_rand($imagesArray)],
             'created_at'  => $date->addWeeks(rand(1, 52))->format('Y-m-d H:i:s'),
             'updated_at' => $date,
             'deleted_at' => NULL

         ];

         Post::create($data);
     } 



 }
}
