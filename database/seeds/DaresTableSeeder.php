<?php

use Illuminate\Database\Seeder;
use App\Data\Models\User;
use Carbon\Carbon;
use Faker\Factory as Faker;

class DaresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$faker = Faker::create();
    	$date = Carbon::now()->toDateTimeString();

    	$imagesArray = config('dummyImages');

        $data = [];

        foreach (range(1, 75) as $index) {

          $userIds = User::inRandomOrder()->limit(2)->pluck('id')->toArray();

          $data = [

           'status' => 'accepted',
           'challenger_id' => $userIds[0],
           'userImage' => $imagesArray[array_rand($imagesArray)],
           'challengerImage' => $imagesArray[array_rand($imagesArray)],
           'user_id' => $userIds[1],
           'created_at' => $date,
           'updated_at' => $date,
           'deleted_at' => NULL

       ];


       \App\Data\Models\Dare::create($data);

   } 


}
}
