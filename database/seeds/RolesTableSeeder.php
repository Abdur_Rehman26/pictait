<?php

use Illuminate\Database\Seeder;
use App\Data\Models\Role;
use Carbon\Carbon;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$date = Carbon::now();     

    	$rolesData = [
    		'id' => 1,
    		'title' => 'User',
    		'scope' => json_encode([
    			"user.index",
    		]),
    		'created_at' => $date,
    		'updated_at' => $date,
    		'deleted_at' => NULL,
    	];

    	Role::insertOnDuplicateKey($rolesData);
    }
}
