<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        if(array_get(request()->server() , 'argv.1') == 'migrate:fresh'){

            $this->call(CountryTableSeeder::class);
            $this->call(UsersTableSeeder::class);
            $this->call(PostsTableSeeder::class);
            $this->call(DaresTableSeeder::class);

            \Artisan::call('passport:install');

            echo "After connection with user sark_92@hotmail.com run \n \nphp artisan db:seed again\n\n";

        }

        $this->call(FollowerTableSeeder::class);
        $this->call(EventTableSeeder::class);

        // $this->call(MessageTableSeeder::class);

        \Artisan::call('cache:clear');



    }
}
