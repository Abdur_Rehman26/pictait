<?php

use Illuminate\Database\Seeder;
use App\Data\Models\User;
use Carbon\Carbon;
use Faker\Factory as Faker;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


    	$faker = Faker::create();
    	$date = Carbon::now();


    	$i =3;

        $imagesArray = config('dummyImages');

        $data[] = [
            'id' => 1,
            'first_name' => 'Syed Abdur Rehman Kazmi',
            'last_name' => '',
            'email' => 'sark_92@hotmail.com',
            'password' => '',
            'created_at' => $date,
            'updated_at' => $date,
            'deleted_at' => NULL,
            'image' => $imagesArray[array_rand($imagesArray)],
            

        ];

        foreach (range(1, 150) as $index) {

          $data[] = [

             'id' => $i,
             'first_name' => $faker->firstName,
             'last_name' =>$faker->LastName,
             'email' => $faker->email,
             'password' => bcrypt('cygnismedia'),
             'created_at' => $date,
             'updated_at' => $date,
             'deleted_at' => NULL,
             'image' => $imagesArray[array_rand($imagesArray)],
         ];
         $i++;
     } 

     User::insertOnDuplicateKey($data, ['first_name', 'last_name', 'updated_at']);

 }
}
