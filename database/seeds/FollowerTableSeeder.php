<?php

use Illuminate\Database\Seeder;
use App\Data\Models\Follower;
use App\Data\Models\User;
use Carbon\Carbon;

class FollowerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
    	$created_at = Carbon::now()->toDateTimeString();

    	$userIds = User::inRandomOrder()->limit(150)->pluck('id')->toArray();

        $requestedArray = [0, 0, 0, 0, 0, 1];


    	foreach ($userIds as $key => $userId) {


    		$followIds = User::where('id' , '!=', $userId)->inRandomOrder()->limit(5)->pluck('id')->toArray();

    		foreach ($followIds as $key => $followId) {

    			$data = [
    				'created_at' => $created_at,
    				'updated_at' => $created_at,
    				'deleted_at' => null,
    				'user_id' => $followId,
    				'follow_id' => $userId,
    				'follow' => true,
                    'type' => 'user',
    				'requested' => $requestedArray[array_rand($requestedArray)],
    			];
                
                app('FollowerRepository')->create($data);

            }

    	   
        }

    	// Follower::insertOnDuplicateKey($data);

    }
}
