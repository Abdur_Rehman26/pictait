<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEventsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('events', function(Blueprint $table)
		{
			$table->bigInteger('id', true);
			$table->string('name');
			$table->enum('status', ['active','archived' ,'pending' , 'closed'])->default('active');
			$table->bigInteger('user_id')->unsigned()->index();
			$table->string('instance_type', 20)->nullable();
			$table->bigInteger('instance_id')->nullable();
			$table->text('image', 65535)->nullable();
			$table->text('description', 65535)->nullable();
			$table->string('event_type')->nullable()->default('competition');
			$table->boolean('number_of_images')->nullable();
			$table->string('privacy_type');
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('events');
	}

}
