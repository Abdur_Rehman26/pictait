<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFollowerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('followers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->enum('type', array('user','event','room'))->default('user');
            $table->bigInteger('user_id')->unsigned()->index();
            $table->boolean('requested')->default(null);
            $table->bigInteger('follow_id')->unsigned()->index();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('NO ACTION');
            $table->foreign('follow_id')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('NO ACTION');
            $table->unique(['user_id', 'follow_id', 'type']);
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('followers');
    }
}
